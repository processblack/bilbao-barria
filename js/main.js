    //var directionsService = new google.maps.DirectionsService();
    var directionDisplay;
    var map;
    var pos;
    var ETG_position;
    
    function initialize(lat, lng) {
        directionsDisplay = new google.maps.DirectionsRenderer();
        var myOptions = {
            zoom: 15,
            center: new google.maps.LatLng(lat, lng),
            mapTypeId: google.maps.MapTypeId.ROADMAP
        }
        map = new google.maps.Map(
            document.getElementById("map"),
            myOptions);
        directionsDisplay.setMap(map);     
            //setMarkers(map, 43.324954946678154, -1.983727455171902);
        ETG_position = new google.maps.LatLng(lat, lng);
        var marker = new google.maps.Marker({
            position: ETG_position,
            map: map,
            //icon: '../img/ubicacion/icono.png',
            title: 'Amenabar Promociones',
            zIndex: 1
        });
    }
    
    // Funcionamiento del menú
    
    $('#boton').click(function(event){
        $("#menu ul.menu").slideToggle();
            $('html, body').animate({
            scrollTop: $("#menu").offset().top
        }, 500);
        $('#bullets').toggle();
        event.preventDefault();
    });
    
    $('body.home #control-menu-escritorio').click(function(){
        $('#menu').toggle('slow');
    });
    
    
    $("li.desplegable > a").click(function(event){
            $(this).find('ul').css('display', 'block');
            event.preventDefault();
    }); 
    
    $(".boxer").boxer();
    $('.selectpicker').selectpicker();

    function ajustar(){
        $('#slide-home, .slide').css('height', ($(window).height()));
        //$('#contenido').css({'margin-top': $(window).height()});
        if ($(window).width() > 768) {
            $('#control-menu-escritorio').css('display', 'block');
            $('body.home #menu').css('display', 'none');
        } else {
            $('#control-menu-escritorio').css('display', 'none');
            $('body.home #menu').css('display', 'block');
            
        }
    }
        
    $(window).scroll(function() {
    
    });
    $(window).resize(function() {
        ajustar();  
    });
    $(window).load(function() {
        ajustar();
    });

/* 
    Envía formularios INSCRIPCIÓN-----------------------------
*/
    $('#enviar_formulario_inscripcion').click(function(event) {
        $('#resultadoMensaje').empty();
        $('#resultadoMensaje').append('<p>Procesando mensaje</p>');
        if ($('#acepto').is(":checked")){
            acepto = "si";
        } else {
            acepto = "no";
        }
        var form_data = {
        // get the form values
            url     : $('#url').val(),
            nombre  : $('#nombre').val(),
            email   : $('#email').val(),
            telefono: $('#telefono').val(),
            acepto  : acepto,
            //habitaciones : $('#habitaciones').val()
			habitaciones: $('input[name="habitaciones"]:checked', '#inscripcion').val()
        };

        // send the form data to the controller
        $.ajax({
            url: "enviar-formulario-inscripcion.php",
            type: "post",
            data: form_data,
            dataType: "json",
            cache: false,
            success: function(msg)
            {
                if(msg.validate) {
                   console.log("ok");
                   $('#resultadoMensaje').empty();
                   $('#resultadoMensaje').append('<p><strong>Gracias por dejar su contacto para nuestra promoción Aldapeta Berri. <br/>A partir del día 29 de abril, se abren nuestras oficinas en la Avenida de la Libertad nº 3, nos pondremos en contacto con usted para concertar una cita en nuestras oficinas.</p>');
                   $('#inscripcion').fadeOut();
                   $('#enviar_formulario_inscripcion').attr('disabled', 'disabled');
				   $('#inscripcion-txt-entrada').attr('visibility', 'hidden');
                } else {
                   console.log(msg.mensaje);
                   $('#resultadoMensaje').append('<p><strong>Error al enviar el mensaje!</strong><br />'+ msg.mensaje +'</p>');
                }
            }
        });
        event.preventDefault();
        // prevents from refreshing the page
    });    

/* 
    Envía formularios CONTACTO -----------------------------
*/	
	
	    $('#enviar_formulario').click(function(event) {
        $('#resultadoMensaje').empty();
        $('#resultadoMensaje').append('<p>Procesando mensaje</p>');
        if ($('#acepto').is(":checked")){
            acepto = "si";
        } else {
            acepto = "no";
        }
        var form_data = {
        // get the form values
            url     : $('#url').val(),
            nombre  : $('#nombre').val(),
            email   : $('#email').val(),
            telefono: $('#telefono').val(),
            acepto  : acepto,
            mensaje : $('#mensaje').val()
        };

        // send the form data to the controller
        $.ajax({
            url: "enviar-formulario-contacto.php",
            type: "post",
            data: form_data,
            dataType: "json",
            cache: false,
            success: function(msg)
            {
                if(msg.validate) {
                   console.log("ok");
                   $('#resultadoMensaje').empty();
                   $('#resultadoMensaje').append('<p><strong>Mensaje enviado!</strong><br />En breve nos pondremos en contacto con usted.</p>');
                   $('#contacto').fadeOut();
                   $('#enviar_formulario').attr('disabled', 'disabled');
                } else {
                   console.log(msg.mensaje);
                   $('#resultadoMensaje').append('<p><strong>Error al enviar el mensaje!</strong><br />'+ msg.mensaje +'</p>');
                }
            }
        });
        event.preventDefault();
        // prevents from refreshing the page
    });    


/* 
    Elige tu vivienda ---------------
*/
    function mostrarPlanos(id){
    		console.log(id);
    		var a1, a2, a3, a4, a5, a6;
    
    		a1 = "";
    		a2 = "";
    		a3 = "";
    		a4 = "";
    		a5 = "";
    		a6 = "";
    		
    		switch (id) {
    			case "1":
    				a1 = 'in active';
    				break;
    			case "2":
    				a2 = 'in active';
    				break;
    			case "3":
    				a3 = 'in active';
    				break;
    			case "4":
    				a4 = 'in active';
    				break;
    			case "5":
    				a5 = 'in active';
    				break;
			}
    
    		$("#portal-1").removeClass();
    		$("#portal-2").removeClass();
    		$("#portal-3").removeClass();
    		$("#portal-4").removeClass();
    		$("#portal-5").removeClass();

    		
    		$("#portal-1").addClass("tab-pane fade " + a1);
    		$("#portal-2").addClass("tab-pane fade " + a2);
    		$("#portal-3").addClass("tab-pane fade " + a3);
    		$("#portal-4").addClass("tab-pane fade " + a4);
    		$("#portal-5").addClass("tab-pane fade " + a5);
    		
    		$(".nav-tabs li").each(function (index) { 
    		
    			$(this).removeClass();			
    			
    			if (index == 0 && id == 1){
    				$(this).addClass ("active");
    			}
    			if (index == 1 && id == 2){
    				$(this).addClass ("active");
    			}
    			if (index == 2 && id == 3){
    				$(this).addClass ("active");
    			}
    			if (index == 3 && id == 4){
    				$(this).addClass ("active");
    			}
    			if (index == 4 && id == 5){
    				$(this).addClass ("active");
    			}
				
				
    		});				
    	return;			
    }
    
    if (document.getElementById("svgMap")){
    var a = document.getElementById("svgMap");
    a.addEventListener("load", function () {
    	//bloque1
    	svgDoc = a.contentDocument;
        delta = svgDoc.getElementById("portal-1"); 
        delta.addEventListener("click", function () { mostrarPlanos("1"); }, false);
    
    	//bloque2
        delta = svgDoc.getElementById("portal-2");
        delta.addEventListener("click", function () { mostrarPlanos("2"); }, false);
    
    	//bloque3
        delta = svgDoc.getElementById("portal-3");
        delta.addEventListener("click", function () { mostrarPlanos("3"); }, false);
    
    	//bloque4
        delta = svgDoc.getElementById("portal-4");
        delta.addEventListener("click", function () { mostrarPlanos("4"); }, false);
    
    	//bloque5
        delta = svgDoc.getElementById("portal-5");
        delta.addEventListener("click", function () { mostrarPlanos("5"); }, false);
		
		
    }, false);
    }

/*
    La obra hoy ------------------------
*/

    function reloadSkillbar(itemSeleccionado){
        // Si no pasa itemSeleccionado (ej, al inicializar) seteamos a #fechas
        if (itemSeleccionado == undefined){
            itemSeleccionado = "#fechas";
        }
    	// Valores y nombre seleccionado
    	var cadena = $(itemSeleccionado).find(":selected").val();
    	var nombre = $(itemSeleccionado).find(":selected").text();
    
        if (itemSeleccionado == "#fechas"){
            $('#fechas_copia option:contains("'+nombre+'")').prop('selected', true);
        } else {
            console.log('abajo');
            $('#fechas option:contains("'+nombre+'")').prop('selected', true);
        }
        
    
    	// genera el nombre de archivo (cambia espacios por guiones)
    	var nombreNiceName = nombre.replace(" ", "-");
    	nombreNiceName = nombreNiceName.toLowerCase();
    	
    	// hacemos un array con los datos
    	var datos = cadena.split(',');
    	
    	// para las fotos, el primer valor.
        $('#fotos').empty();
        $('#miniaturas').empty();
    	for ( var i = 0; i < datos[0]; i++ ) {
        	// Para mostrar la principal y ocultar las demás
        	//$('#fotos').append('<li><a href="../img/la-obra-hoy/fotos/'+nombreNiceName+'/'+i+'.jpg" class="boxer"  data-gallery="gallery"><img src="../img/la-obra-hoy/fotos/'+nombreNiceName+'/'+i+'.jpg" alt="TEST" class="img-responsive center-block" /></a></li>');
        	$('#fotos').append('<li><img src="../img/la-obra-hoy/fotos/'+nombreNiceName+'/'+i+'.jpg" alt="TEST" class="img-responsive center-block" /></li>');
        	
        	$('#miniaturas').append('<li><a href="../img/la-obra-hoy/fotos/'+nombreNiceName+'/'+i+'.jpg"><img src="../img/la-obra-hoy/fotos/'+nombreNiceName+'/m-'+i+'.jpg" alt="TEST" class="img-responsive" /></a></li>');
        }
        
        //$('.cycle-slideshow').cycle('reinit');
        // La ventana modal
        $(".boxer").boxer();
        
        // las barras estadísticas
    	var i = 1;
        $('.skillbar').each(function(){
            $(this).find('.skillbar-bar').animate({
        		width:datos[i].toString()+'%'
        	},2000); // 2000 indica el tiempo de la animación
        	$(this).find('.skill-bar-percent').text(datos[i].toString()+'%');
        	i++;
        });
        
        
        
        
        // Carrusel sincronizado de la ficha de promociones
        // http://sorgalla.com/jcarousel/
            var connector = function(itemNavigation, carouselStage) {
                return carouselStage.jcarousel('items').eq(itemNavigation.index());
            };
        
            $(function() {
                var carouselStage = $('.carousel-stage').on('jcarousel:create jcarousel:reload', function() {
                var element = $(this),
                    width = element.innerWidth();
    
                // This shows 1 item at a time.
                // Divide `width` to the number of items you want to display,
                // eg. `width = width / 3` to display 3 items at a time.
                element.jcarousel('items').css('width', width + 'px');
            })
            .jcarousel();
                var carouselNavigation = $('.carousel-navigation').jcarousel();
        
                carouselNavigation.jcarousel('items').each(function() {
                    var item = $(this);
        
                    var target = connector(item, carouselStage);
                    console.log(item);
                    item
                        .on('jcarouselcontrol:active', function() {
                            carouselNavigation.jcarousel('scrollIntoView', this);
                            item.addClass('active');
                        })
                        .on('jcarouselcontrol:inactive', function() {
                            item.removeClass('active');
                        })
                        .jcarouselControl({
                            target: target,
                            carousel: carouselStage
                        });
                });
        
                $('.prev-stage')
                    .on('jcarouselcontrol:inactive', function() {
                        $(this).addClass('inactive');
                    })
                    .on('jcarouselcontrol:active', function() {
                        $(this).removeClass('inactive');
                    })
                    .jcarouselControl({
                        target: '-=1'
                    });
        
                $('.next-stage')
                    .on('jcarouselcontrol:inactive', function() {
                        $(this).addClass('inactive');
                    })
                    .on('jcarouselcontrol:active', function() {
                        $(this).removeClass('inactive');
                    })
                    .jcarouselControl({
                        target: '+=1'
                    });
        
                // Setup controls for the navigation carousel
                $('.prev-navigation')
                    .on('jcarouselcontrol:inactive', function() {
                        $(this).addClass('inactive');
                    })
                    .on('jcarouselcontrol:active', function() {
                        $(this).removeClass('inactive');
                    })
                    .jcarouselControl({
                        target: '-=1'
                    });
        
                $('.next-navigation')
                    .on('jcarouselcontrol:inactive', function() {
                        $(this).addClass('inactive');
                    })
                    .on('jcarouselcontrol:active', function() {
                        $(this).removeClass('inactive');
                    })
                    .jcarouselControl({
                        target: '+=1'
                    });
            });
        // Fin carrusel sincronizado
    }
    reloadSkillbar();
    
    // Cuando cambia el de arriba
    $('#fechas').change(function (){
        reloadSkillbar('#fechas');
    });
    // Cuando cambia el de abajo
    $('#fechas_copia').change(function (){
        reloadSkillbar('#fechas_copia');
    });
    


    




    
