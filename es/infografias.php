<?php include('inc/init.php'); ?>
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        
        <title>Bilbo Barria - Contacto</title>
        
        <link href='https://fonts.googleapis.com/css?family=Lato:300,400|Raleway' rel='stylesheet' type='text/css'>
        <link href="<?php echo $base_url; ?>/css/bootstrap.min.css" rel="stylesheet">
        <link href="<?php echo $base_url; ?>/css/main.css" rel="stylesheet">
        
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        
    </head>
    <body>
    <?php 
        $seccion = "aviso-legal";
        include('inc/cabecera.php');
    ?>
    
    <article id="contenido" class="legal">
        <div class="container">
        	<div class="row">
        	    <div class="col-md-6 col-md-offset-3">
        	    	<h2 class="text-uppercase text-center">Infografías</h2>
        	    </div><!-- .col-md-6 -->
        	</div><!-- .row -->
        	
        	<div class="row">
        	    <div class="col-md-12">
                    <p>Las infografías de las fachadas, elementos comunes y restantes espacios son orientativas y podrán ser objeto de variación o modificación en los proyectos técnicos. El mobiliario de las infografías interiores no está incluido y el equipamiento de las viviendas será el indicado en la correspondiente memoria de calidades.</p>
                    <p><br /><br /><br /><br /></p>
                </div><!-- .col-md-12 -->
        	</div><!-- .row -->
        </div><!-- .container -->
    </article>
    <?php include('inc/pie.php'); ?>
  </body>
</html>





