<?php include('inc/init.php'); ?>
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        
        <title>Bilbo Barria - Inscripciones</title>
        
        <link href='https://fonts.googleapis.com/css?family=Lato:300,400|Raleway' rel='stylesheet' type='text/css'>
        <link href="<?php echo $base_url; ?>/css/bootstrap.min.css" rel="stylesheet">
        <link href="<?php echo $base_url; ?>/css/main.css" rel="stylesheet">
        
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        
        <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=true"></script>
    </head>
    <body onload="initialize(43.3145801,-1.9872902)">
    <?php 
        $seccion = "inscripcion";
        include('inc/cabecera.php');
    ?>
    
    <article id="contenido" class="contacto">
		<div class="container">
		
			<div class="row">
        	    <div class="col-md-6 col-md-offset-3">
        	    	<h2 class="text-center">Formulario de inscripción</h2>
        	    </div><!-- .col-md-6 -->
				<div class="col-md-6 col-md-offset-3" id="inscripcion-txt-entrada">
                    <p>Inscríbase y nos pondremos en contacto con usted para concertar una cita en nuestras oficinas.</p>
                </div>
        	</div><!-- .row -->
			
        	<div class="row">
        	    <div class="col-md-6 col-md-offset-3">
        	    	<form id="inscripcion" name="inscripcion" method="post" action="">
        	    		<p>
        	    			<label for="nombre">Nombre y apellidos</label>
        	    			<input type="text" name="nombre" id="nombre" class="form-control" />
        	    		</p>
        	    		<p>
        	    			<label for="email">E-mail</label>
        	    			<input type="text" name="email" id="email" class="form-control"  />
        	    		</p>
        	    		<p>
        	    			<label for="telefono">Teléfono</label>
        	    			<input type="text" name="telefono" id="telefono" class="form-control"  />
        	    		</p>
        	    		<p>
        	    			<label for="habitaciones">Preferencias</label>							
							<div class="radio">
								<label><input type="radio" name="habitaciones" value="2 habitaciones">2 habitaciones&nbsp;</label>
								<label><input type="radio" name="habitaciones" value="3 habitaciones" checked>3 habitaciones&nbsp;</label>
							</div>
        	    		</p>
						
        	    		<p>
        	    		    <input type="checkbox" name="acepto" id="acepto" value="si"> He leído y acepto la <a href="<?php echo $base_url; ?>/<?php echo $idioma; ?>/politica-de-privacidad.php" target="_blank">Política de privacidad</a>.
        	    		 </p>
						<br/>
        	    		<p>
        	    			<input type="submit" name="enviar" id="enviar_formulario_inscripcion" value="Enviar" class="btn btn-default" />
        	    		</p>
        	    	</form>
        	    	<div id="resultadoMensaje"></div>
        	    </div><!-- .col-md-10 -->
        	</div><!-- .row -->	
		</div>
    </article>
    <?php include('inc/pie.php'); ?>
  </body>
</html>





