    <footer id="pie" class="fondo-gris">
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                	<p class="centrado-xs">
					
					<a href="http://www.amenabarpromociones.com"><img src="<?php echo $base_url; ?>/img/layout/amenabar-residenciales.png" alt="Amenabar promociones residenciales" width="239" height="49"></a>
					
					</p>
                </div><!-- .col-md-4 -->
                <div class="col-md-4">
                	<address class="centrado-xs">
                    	Alameda de Urquijo, 63<br />
                        48003 Bilbao<br />
                        944.790.600<br />
                        <a href="mailto:<?=$emailcomercial?>"><?=$emailcomercial?></a>
                    </address>
                </div><!-- .col-md-2 -->
                <div class="col-md-2 col-xs-6">
                	<p>
                    	<a href="<?php echo $base_url; ?>/<?php echo $idioma; ?>/politica-de-privacidad.php">Política de privacidad</a><br />
                    	<a href="<?php echo $base_url; ?>/<?php echo $idioma; ?>/aviso-legal.php">Aviso legal</a><br />
                        <a href="<?php echo $base_url; ?>/<?php echo $idioma; ?>/cookies.php">Cookies</a><br />
                        <a href="<?php echo $base_url; ?>/<?php echo $idioma; ?>/infografias.php">Aviso infografías</a>
                    </p>
                </div><!-- .col-md-2 -->
                <div class="col-md-2 col-xs-6">
                	<!--<p class="text-right visible-lg visible-md"><img src="<?php echo $base_url; ?>/img/layout/una-empresa-amenabar.png" alt="Una empresa amenabar"></p>-->
                	<!--<p class="text-right visible-sm visible-xs"><img src="<?php echo $base_url; ?>/img/layout/una-empresa-amenabar-xs.png" alt="Una empresa amenabar"></p>-->
					
				
                </div><!-- .col-md-4 -->
            </div><!-- .row -->
        </div><!-- .container -->
    </footer>    

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>

    <script src="<?php echo $base_url; ?>/js/bootstrap.min.js"></script>
    <script src="<?php echo $base_url; ?>/js/plugins.js"></script>
    <script src="<?php echo $base_url; ?>/js/main.js"></script>



    <script type="text/javascript" id="cookiebanner" src="<?php echo $base_url; ?>/js/cookiebanner.min.js" data-bg="#555" data-fg="#fff" data-link="#fff" data-position="bottom" data-moreinfo="<?php echo $base_url; ?>/<?php echo $idioma; ?>/cookies.php" data-message="Este sitio web utiliza archivos cookies propios y de terceros para mejorar nuestros servicios y adaptar el sitio web a sus hábitos de navegación. Si continúa navegando se entenderá que acepta su uso." data-linkmsg="Más información sobre el uso de cookies"></script>
	
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-26009289-30', 'auto');
  ga('send', 'pageview');

</script>