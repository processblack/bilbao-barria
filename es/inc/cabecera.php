    <div id="idioma">
        <div class="container">
        	<div class="row">
        	    <div class="col-md-12">
                    <p class="text-right">
                        <?php
                            $url = $_SERVER['PHP_SELF'];
                        ?>
						<!--
						<a href="<?php echo str_replace('/'.$idioma.'/', '/es/', $url); ?>">ES</a> |  
                        <a href="<?php echo str_replace('/'.$idioma.'/', '/eu/', $url); ?>">EU</a>
						-->

                    </p>
        	    </div><!-- .col-md-12 -->
        	</div><!-- .row -->
        </div><!-- .container -->
    </div>

    <header id="cabecera">
        <div class="container">
            <h1 id="logo">
                <a href="<?php echo $base_url; ?>/<?php echo $idioma; ?>/"><img src="<?php echo $base_url; ?>/img/layout/logo.png" srcset="<?php echo $base_url; ?>/img/layout/logo@2x.png 2x" alt="logo" width="100%" height="100%"></a>
            </h1>
            <div id="control-menu-escritorio">
                <span class="linea"></span>
                <span class="linea"></span>
                <span class="linea"></span>
            </div>
            <nav id="menu">
                <div id="boton" class="visible-xs"><img src="<?php echo $base_url; ?>/img/iconos/menu.png" alt="menu" width="44" height="44" /></div>
                <ul class="menu">
                    <li<?php echo ($seccion == 'index' ? ' class="current"' : ''); ?>><a href="<?php echo $base_url; ?>/<?php echo $idioma; ?>/index.php">Inicio</a></li>
                    <li<?php echo ($seccion == 'ubicacion' ? ' class="current"' : ''); ?>><a href="<?php echo $base_url; ?>/<?php echo $idioma; ?>/ubicacion.php">Ubicación</a></li>
					
                    <li<?php echo ($seccion == 'viviendas' ? ' class="current"' : ''); ?>><a href="<?php echo $base_url; ?>/<?php echo $idioma; ?>/viviendas/caracteristicas.php">Viviendas</a>
                        <ul>
                            <li<?php echo ($subSeccion == 'caracteristicas' ? ' class="current"' : ''); ?>><a href="<?php echo $base_url; ?>/<?php echo $idioma; ?>/viviendas/caracteristicas.php">Características</a></li>
<!--                            <li<?php echo ($subSeccion == 'galeria' ? ' class="current"' : ''); ?>><a href="<?php echo $base_url; ?>/<?php echo $idioma; ?>/viviendas/galeria.php">Imágenes</a></li>-->
                            <li<?php echo ($subSeccion == 'elige-tu-vivienda' ? ' class="current"' : ''); ?>><a href="<?php echo $base_url; ?>/<?php echo $idioma; ?>/viviendas/elige-tu-vivienda.php">Elija su vivienda</a></li>
							<li<?php echo ($subSeccion == 'personalizacion' ? ' class="current"' : ''); ?>><a href="<?php echo $base_url; ?>/<?php echo $idioma; ?>/viviendas/personalizacion.php">Personalización</a>							
                            <li<?php echo ($subSeccion == 'forma-de-pago' ? ' class="current"' : ''); ?>><a href="<?php echo $base_url; ?>/<?php echo $idioma; ?>/viviendas/forma-de-pago.php">Forma de pago</a></li>
                            <li<?php echo ($subSeccion == 'memoria-calidades' ? ' class="current"' : ''); ?>><a href="<?php echo $base_url; ?>/<?php echo $idioma; ?>/viviendas/memoria-calidades.php">Memoria de Calidades</a></li>
                            
                        </ul>
                    </li>
					<li<?php echo ($seccion == 'galeria' ? ' class="current"' : ''); ?>><a href="<?php echo $base_url; ?>/<?php echo $idioma; ?>/viviendas/galeria.php">Imágenes</a></li>
                   
<!--                   
				   <li<?php echo ($seccion == 'la-obra-hoy' ? ' class="current"' : ''); ?>><a href="<?php echo $base_url; ?>/<?php echo $idioma; ?>/la-obra-hoy.php">La obra hoy</a></li>
-->					
<!--                    <li<?php echo ($seccion == 'area-clientes' ? ' class="current"' : ''); ?>><a href="<?php echo $base_url; ?>/<?php echo $idioma; ?>/area-clientes/index.php">Área clientes</a></li>-->
<!--                    <li<?php echo ($seccion == 'constructora' ? ' class="current"' : ''); ?>><a href="<?php echo $base_url; ?>/<?php echo $idioma; ?>/constructora.php">Constructora</a></li>-->
                    <li class="current"><a href="<?php echo $base_url; ?>/<?php echo $idioma; ?>/cita.php">Reserva tu Cita</a></li>
					<li<?php echo ($seccion == 'contacto' ? ' class="current"' : ''); ?>><a href="<?php echo $base_url; ?>/<?php echo $idioma; ?>/contacto.php">Contacto</a></li>
                </ul>
            </nav>
        </div>
    </header>
