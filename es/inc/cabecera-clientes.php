    <div id="idioma">
        <div class="container">
        	<div class="row">
        	    <div class="col-md-12">
                    <p class="text-right">
                        <?php
                            $url = $_SERVER['PHP_SELF'];
                        ?>
                        <a href="<?php echo str_replace('/'.$idioma.'/', '/es/', $url); ?>">ES</a> |  
                        <a href="<?php echo str_replace('/'.$idioma.'/', '/eu/', $url); ?>">EU</a> | 
                        <a href="<?php echo str_replace('/'.$idioma.'/', '/fr/', $url); ?>">FR</a>
                    </p>
        	    </div><!-- .col-md-12 -->
        	</div><!-- .row -->
        </div><!-- .container -->
    </div>

    <header id="cabecera">
        <div class="container">
            <h1 id="logo">
                <a href="<?php echo $base_url; ?>/<?php echo $idioma; ?>/"><img src="<?php echo $base_url; ?>/img/layout/logo.png" alt="logo" width="155" height="85"></a>
            </h1>
            <div id="datos-cliente">
                <p>
                    <strong>Bienvenido, ...</strong><br />
                    Aldapeta Berri, San Sebastián<br />
                    
                </p>
            </div>
            <nav id="menu" class="privada">
                <div id="boton" class="visible-xs"><img src="<?php echo $base_url; ?>/img/iconos/menu.png" alt="menu" width="44" height="44" /></div>
                <ul class="menu">
                    <li<?php echo ($seccion == 'inicio' ? ' class="current"' : ''); ?>><a href="<?php echo $base_url; ?>/<?php echo $idioma; ?>/area-clientes/index-2.php">Inicio</a></li>
                    <li<?php echo ($seccion == 'personaliza' ? ' class="current"' : ''); ?>><a href="<?php echo $base_url; ?>/<?php echo $idioma; ?>/area-clientes/personaliza.php">Personaliza tu vivienda</a></li>
                    <li class="desconectar"><a href="<?php echo $base_url; ?>/<?php echo $idioma; ?>/area-clientes/index.php" class="btn btn-primary">Desconectar</a></li>
                </ul>
            </nav>
        </div>
    </header>
