<?php include('../inc/init.php'); ?>
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        
        <title>Bilbo Barria</title>
        
        <link href='https://fonts.googleapis.com/css?family=Lato:300,400|Raleway' rel='stylesheet' type='text/css'>
        <link href="<?php echo $base_url; ?>/css/bootstrap.min.css" rel="stylesheet">
        <link href="<?php echo $base_url; ?>/css/main.css" rel="stylesheet">
        
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
    <?php 
        $seccion = "viviendas";
        $subSeccion = "memoria-calidades";
        include('../inc/cabecera.php');
    ?>
    
    <article id="contenido" class="viviendas-forma-de-pago">
        <div class="container">    
            <div class="row">
                <div class="col-md-12">
                	<h2 class="text-uppercase text-center">Memoria de Calidades</h2>
                </div><!-- .col-md-12 -->
            </div><!-- .row -->

			<div>
            <div class="container">
                <div class="row">
                    <div class="col-md-4 col-md-offset-2">
                    	<p class="text-center">
                        	<a target="_blank" href="../../descargas/memoria-de-calidades.pdf"><img src="<?php echo $base_url; ?>/img/iconos/calidades.png" alt="Descargar memoria de calidades" width="74" height="74"><br />
                        	Descargar memoria de calidades</a>
                        </p>
                    </div><!-- .col-md-6 -->
                    <div class="col-md-4">
                    	<p class="text-center">
                        	<a target="_blank" href="../../descargas/folleto-informativo.pdf"><img src="<?php echo $base_url; ?>/img/iconos/folleto.png" alt="Descargar folleto informativo" width="74" height="74"><br />
                        	Descargar folleto informativo</a>
                        </p>
                    </div><!-- .col-md-6 -->
                </div>            	
            </div><!-- .container -->    
        </div><!-- .fondo-gris -->
			
			
        </div><!-- .container -->
    </article>
    <?php include('../inc/pie.php'); ?>
  </body>
</html>





