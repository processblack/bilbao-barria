<?php include('../inc/init.php'); ?>
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        
        <title>Bilbo Barria</title>
        
        <link href='https://fonts.googleapis.com/css?family=Lato:300,400|Raleway' rel='stylesheet' type='text/css'>
        <link href="<?php echo $base_url; ?>/css/bootstrap.min.css" rel="stylesheet">
        <link href="<?php echo $base_url; ?>/css/main.css" rel="stylesheet">
        
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

    </head>
    <body>
    <?php 
        $seccion = "viviendas";
        $subSeccion = "elige-tu-vivienda";
        include('../inc/cabecera.php');
    ?>
    
    <article id="contenido" class="viviendas-tu-vivienda">
        <div class="container">    
            <div class="row">
			
			
             
				<div class="col-md-12">
                	<div id="infografia">
                        <object id="svgMap" type="image/svg+xml" data="<?php echo $base_url; ?>/img/elige-tu-vivienda/infografia.svg" class="responsive">
						<img id="fotografia" src="<?php echo $base_url; ?>/img/elige-tu-vivienda/infografia.jpg" alt="viviendas" class="img-responsive">
						</object>
                    </div>
                </div><!-- .col-md-12 -->
			
			

				<!-- .col-md-12 -->
				
            </div><!-- .row -->
            
            
            <div class="row">
                <div class="col-md-12">
                	<ul class="nav nav-tabs" role="tablist">
                	    <li role="presentation" class="active"><a href="#portal-1" aria-controls="portal-1" role="tab" data-toggle="tab">Portal 1</a></li>								
                	    <li role="presentation"><a href="#portal-2" aria-controls="portal-2" role="tab" data-toggle="tab">Portal 2</a></li>
						
                	</ul>
                </div><!-- .col-md-12 -->
            </div><!-- .row -->
            <div class="row">
                <div class="col-md-12">
                    <div class="tab-content">                        			
						<div role="tabpanel" class="tab-pane active" id="portal-1">
						<div class=""> 
                        <table class="table table-striped">
                        	<thead>
                        		<tr>
                            		<td colspan="6">
                                        <div class="row">
                                            <div class="col-md-12">
                                            	<!--<p class="text-center empuja"><strong>Fecha de inicio obras</strong>: - · <strong>Fecha fin de obras</strong>: -</p>-->
                                            </div><!-- .col-md-12 -->
                                        </div><!-- .row -->
                            		    </td>
                    		    </tr>
                        		<tr>
                        			<th class="mayusculas">Portal</th>
                        			<th class="mayusculas">Planta</th>
                        			<th class="mayusculas">Mano</th>
                        			<th class="mayusculas">Precio</th>
                        			<th class="mayusculas">Plano</th>
                        			<th class="mayusculas">Estado</th>
                        		</tr>
                        	</thead>
                                 <tbody>
									
									<tr>
										<td>#1</td>
										<td>BAJA</td>
										<td>A</td>
										<td></td>
										<td><a href="../../descargas/planos/P1-BA.pdf" target="_blank">descargar</a></td>
										<td><a href="#" class="btn btn-success btn-xs">Disponible</a></td>
									</tr>
									
									<tr>
										<td>#1</td>
										<td>19</td>
										<td>A</td>
										<td></td>
										<td><a href="../../descargas/planos/P1-BA.pdf" target="_blank">descargar</a></td>
										<td><a href="#" class="btn btn-success btn-xs">Disponible</a></td>
									</tr>
						
								</tbody>
                            </table>                            
                        </div><!-- # Portal 1 -->						
						</div>	
			

						
					</div><!-- .col-md-12 -->
				</div><!-- .row -->
			</div><!-- .container -->
    </article>
    <?php include('../inc/pie.php'); ?>
  </body>
</html>





