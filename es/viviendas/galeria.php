<?php include('../inc/init.php'); ?>
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        
        <title>Bilbo Barria</title>
        
        <link href='https://fonts.googleapis.com/css?family=Lato:300,400|Raleway' rel='stylesheet' type='text/css'>
        <link href="<?php echo $base_url; ?>/css/bootstrap.min.css" rel="stylesheet">
        <link href="<?php echo $base_url; ?>/css/main.css" rel="stylesheet">
        
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
    <?php 
        $seccion = "galeria";
        $subSeccion = "galeria";
        include('../inc/cabecera.php');
    ?>
    
    <article id="contenido" class="viviendas-caracteristicas">
        <div class="container">    
            <div class="row">
                <div class="col-md-12">
					<p><img src="<?php echo $base_url; ?>/img/galeria/01.jpg" alt="Fotografías viviendas  1" class="img-responsive"></p>
                	<p><img src="<?php echo $base_url; ?>/img/galeria/02.jpg" alt="Fotografías viviendas  2" class="img-responsive"></p>
                	<p><img src="<?php echo $base_url; ?>/img/galeria/03.jpg" alt="Fotografías viviendas  3" class="img-responsive"></p>				
                	<p><img src="<?php echo $base_url; ?>/img/galeria/04.jpg" alt="Fotografías viviendas  4" class="img-responsive"></p>
                	<p><img src="<?php echo $base_url; ?>/img/galeria/05.jpg" alt="Fotografías viviendas  5" class="img-responsive"></p>
                	<p><img src="<?php echo $base_url; ?>/img/galeria/06.jpg" alt="Fotografías viviendas  6" class="img-responsive"></p>
                	<p><img src="<?php echo $base_url; ?>/img/galeria/07.jpg" alt="Fotografías viviendas  7" class="img-responsive"></p>
                	<p><img src="<?php echo $base_url; ?>/img/galeria/08.jpg" alt="Fotografías viviendas  8" class="img-responsive"></p>
                	<p><img src="<?php echo $base_url; ?>/img/galeria/09.jpg" alt="Fotografías viviendas  9" class="img-responsive"></p>
                	<p><img src="<?php echo $base_url; ?>/img/galeria/10.jpg" alt="Fotografías viviendas  10" class="img-responsive"></p>
                	<p><img src="<?php echo $base_url; ?>/img/galeria/11.jpg" alt="Fotografías viviendas  11" class="img-responsive"></p>
                	<p><img src="<?php echo $base_url; ?>/img/galeria/12.jpg" alt="Fotografías viviendas  12" class="img-responsive"></p>
                	<p><img src="<?php echo $base_url; ?>/img/galeria/13.jpg" alt="Fotografías viviendas  13" class="img-responsive"></p>
                	<p><img src="<?php echo $base_url; ?>/img/galeria/14.jpg" alt="Fotografías viviendas  14" class="img-responsive"></p>

					</div><!-- .col-md-12 -->
            </div><!-- .row -->
        </div>
    </article>
    <?php include('../inc/pie.php'); ?>
  </body>
</html>





