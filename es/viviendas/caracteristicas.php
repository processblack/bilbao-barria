<?php include('../inc/init.php'); ?>
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        
        <title>Bilbo Barria</title>
        
        <link href='https://fonts.googleapis.com/css?family=Lato:300,400|Raleway' rel='stylesheet' type='text/css'>
        <link href="<?php echo $base_url; ?>/css/bootstrap.min.css" rel="stylesheet">
        <link href="<?php echo $base_url; ?>/css/main.css" rel="stylesheet">
        
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
    <?php 
        $seccion = "viviendas";
        $subSeccion = "caracteristicas";
        include('../inc/cabecera.php');
    ?>
    
    <article id="contenido" class="viviendas-caracteristicas">
        <div class="container">    
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                	<h2 class="text-center text-uppercase">96 viviendas libres </h2>
                	<p class="text-center">La Promoción Bilbo Barria se encuentra en una ubicación privilegiada y estratégica, ya que es uno de los mejores ámbitos de Bilbao que ofrece las mejores conexiones y posibilidades de transporte</p>
                </div><!-- .col-md-8 -->
            </div>
            <div class="row">
                <div class="col-md-12">
                	<p><img src="<?php echo $base_url; ?>/img/caracteristicas/02.jpg" alt="01" width="1500" height="850" class="img-responsive"></p>
                </div><!-- .col-md-12 -->
            </div><!-- .row -->
            
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                	<h2 class="text-center text-uppercase">Un proyecto arquitectónico moderno y respetuoso con el entorno</h2>
                	<p class="text-center margen-inferior">
                    	El nuevo edificio enclavado en lo alto del municipio, pasara a formar parte del nuevo skyline de la ciudad. Parte de un prisma limpio, que aglutina todas las plantas y está acabado con una fachada acristalada. En este prisma se ha insertado un segundo volumen volado, formalizándose diferentes bandejas que continúan los forjados, que conforman las plantas de viviendas. De esta manera, se forman entrantes y salientes en dicha piel que generan sombras que dan ritmo a la fachada, protegiendo las mismas del soleamiento y las inclemencias.

						<br />

                        •<br />
                        <strong>Piscina de adultos</strong> 90m2, desbordante con iluminación nocturna y cloración salina.<br />
                        •<br />
                        <strong>Gimnasio equipado</strong> Tener un gimnasio debajo de tu casa y con vistas a la piscina es la forma más cómoda  y agradable de estar siempre en plena forma<br />
						•<br />
                        <strong>Sociedad gastronómica</strong> Amplio txoko amueblado y equipado. Un espacio pensado para disfrutar de los cumpleaños de tus hijos y cenas con amigos. 
						<br />
                        •<br />
                        <strong>Chill out</strong> comunitario con zonas verdes. Espacio de 45 m2, con vistas a la piscina, destinado al ocio. Una zona equipada que contará con suelo efecto madera. <br />
                                              
           
												
                    </p>
					
                </div><!-- .col-md-8 -->
            </div>
			
            <div class="row">
                <div class="col-md-12">
                	<p><img src="<?php echo $base_url; ?>/img/caracteristicas/15.jpg" alt="01" width="1500" height="850" class="img-responsive"></p>
                </div><!-- .col-md-12 -->
            </div><!-- .row -->

			<div class="row">
                <div class="col-md-8 col-md-offset-2">
                	<h2 class="text-center text-uppercase">Materiales y acabados a la altura del proyecto</h2>
                	<p class="text-center margen-inferior">
                    	El revestimiento exterior del edificio estará compuesto por una fachada ventilada cerámica de gran formato y la cubierta estará acabada con solado de gres con efecto madera. Además, las viviendas contarán con amplias terrazas con barandilla de vidrio y acero inoxidable. La carpintería exterior será de aluminio con rotura de puente térmico y doble acristalamiento con gas argón.

						<br />
						
						La puerta de entrada de las viviendas será blindada y la carpintería interior lacada en blanco. El sistema de calefacción se realizará mediante suelo radiante y termostato programable digital con control independiente de temperatura por estancias.Las cocinas estarán totalmente equipadas con electrodomésticos de 1ª calidad y máxima eficiencia energética. Los dormitorios contarán con armarios empotrados lacados en blanco y distribución interior con balda maletero y barra metálica.
                                              
           
												
                    </p>
					
                </div><!-- .col-md-8 -->
            </div>			
			
			
           		
			

            <div class="row">
                <div class="col-md-12">
                	<p><img src="<?php echo $base_url; ?>/img/caracteristicas/16.jpg" alt="01" width="1500" height="850" class="img-responsive"></p>
                </div><!-- .col-md-12 -->
            </div><!-- .row -->

			
			
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                	<h2 class="text-center text-uppercase">Edificios con la máxima calificación energética</h2>
                	<p class="text-center margen-inferior">
                    	Un conjunto de viviendas sostenibles va más allá de la eficiencia energética, ya que contribuye a cuidar la salud y el bienestar de nuestros clientes y protege el medioambiente. El máximo certificado energético supone una disminución de emisiones de CO2 y una reducción significativa en la demanda energética del edificio y un gran ahorro en su facturación de calefacción y luz.

               <div class="col-md-12">  
					<p class="text-center margen-inferior">			   
                       
                        •<br />
                        <strong>Fachada ventilada</strong> Consiste en la separación entre los ambientes del interior y del exterior de una edificación, por la que discurre una corriente de aire que reduce la humedad. Evita la formación de condensaciones sobre la cara interna de la pared manteniendo unas condiciones óptimas del aislante. 
En verano, consigue que parte del calor radiante se vea reflejado al exterior y la parte del calor que llega a la cámara de aire, active el efecto chimenea. Por otro lado, en invierno, la cámara actúa como acumulador de calor interior
<br />
                        •<br />
                        <strong>mejora de envolvente térmica</strong> Se ha cuidado la envolvente térmica, evitando puentes térmicos y prestando una gran importancia al aislamiento térmico.Garantizando la resistencia al paso del frío desde el exterior, al interior de la vivienda.
La incorporación de carpinterías con rotura de puente térmico, sistemas de doble acristalamiento tipo Climalit con cámara de gas argón y vidrios  con un factor solar bajo, o de baja emisividad, reducen notablemente la carga que por radiación solar, pueda entrar al interior del edificio
<br />
						•<br />
						<strong>Caldera de condensación</strong> El sistema de caldera de condensación centralizada para producción de ACS y calefacción,  utilizará  el calor latente condensando el vapor de agua de los humos, aprovechando la energía, reduciendo el consumo de combustible y la emisión de sustancias nocivas.  
Además, la mejora del rendimiento y la vida útil de la caldera, en comparación con las calderas convencionales, es considerable. Con las calderas de condensación, se conseguirá un ahorro de hasta un 30% en la factura de gas, al recuperar el gas perdido para mantener la temperatura en tu casa.


<br />
						•<br />
                        <strong>Aerotermia</strong> El edificio estará dotado de un sistema de aerotermia que intercambiará el calor entre el sistema y el aire del entorno. La bomba de calor aerotermica, absorbe y recupera la energía del entorno del aire y transfiere el calor agua del circuito para la producción de ACS.  
Para este proceso, no emite humos, ni produce combustión. Con este sistema se conseguirá extraer hasta un 70% de la energía del aire, por lo tanto, consume únicamente el 30% restante, suponiendo un gran ahorro económico.

<br />
						•<br />
                        <strong>Suelo radiante</strong> El sistema de calefacción proyectado es un sistema de calefacción invisible y limpio por suelo radiante de alta eficiencia energética con las siguientes ventajas: 
- Sistema de climatización domótico programable, con termostato independiente  en diferentes zonas de día y noche de la vivienda, adaptando la temperatura al confort  de cada usuario. <br/>
- Trabaja a temperatura más bajas, favoreciendo el incremento del rendimiento de la caldera.<br/>
- Sistema de gran inercia térmica a base de calor estable y homogéneo eliminando corrientes de aire. <br/>
- Misma sensación de confort que con un sistema convencional a menor temperatura ambiente. <br/>
- Ahorro estimado de un 30% sobre una climatización estándar. Funciona a temperaturas de impulsión de 35-40º, en vez de a temperaturas altas 70-80º de un sistema tradicional de radiadores.<br/>

<br />
						
						</p>
               
                </div><!-- .col-md-4 -->
				
                    </p>
                </div><!-- .col-md-8 -->
            </div>

            <div class="row">
                <div class="col-md-12">
                	<p><img src="<?php echo $base_url; ?>/img/caracteristicas/11.jpg" alt="01" width="1500" height="850" class="img-responsive"></p>
                </div><!-- .col-md-12 -->
            </div><!-- .row -->
			
            
        </div><!-- .container -->
        <div class="fondo-gris">
            <div class="container">
                <div class="row">
					<!--
                    <div class="col-md-4 col-md-offset-2">
                    	<p class="text-center">
                        	<a target="_blank" href="../../descargas/memoria-de-calidades.pdf"><img src="<?php echo $base_url; ?>/img/iconos/calidades.png" alt="Descargar memoria de calidades" width="74" height="74"><br />
                        	Descargar memoria de calidades</a>
                        </p>
                    </div>
					-->
					<!-- .col-md-6 -->
				
                    <div class="col-md-12">
                    	<p class="text-center">
                        	<a target="_blank" href="../../descargas/folleto-informativo.pdf"><img src="<?php echo $base_url; ?>/img/iconos/folleto.png" alt="Descargar folleto informativo" width="74" height="74"><br />
                        	Descargar folleto informativo</a>
                        </p>
                    </div><!-- .col-md-6 -->
                </div>            	
            </div><!-- .container -->    
        </div><!-- .fondo-gris -->
    </article>
    <?php include('../inc/pie.php'); ?>
  </body>
</html>





