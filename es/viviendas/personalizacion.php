<?php include('../inc/init.php'); ?>
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        
        <title>Bilbo Barria - Personalización</title>
        
        <link href='https://fonts.googleapis.com/css?family=Lato:300,400|Raleway' rel='stylesheet' type='text/css'>
        <link href="<?php echo $base_url; ?>/css/bootstrap.min.css" rel="stylesheet">
        <link href="<?php echo $base_url; ?>/css/main.css" rel="stylesheet">
        
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        
    </head>
    <body>
    <?php 
        $seccion = "viviendas";
        $subSeccion = "personalizacion";
        include('../inc/cabecera.php');
    ?>
    
    <article id="contenido" class="personalizacion">
        <div class="fondo-gris">
            <div class="container"> 
                <div class="row">
                    <div class="col-md-6 col-md-offset-3">
                    	<h2 class="text-center text-uppercase">Puedes personalizar la vivienda a tu gusto</h2>
                    </div><!-- .col-md-6 -->
                </div><!-- .row -->   
                
            </div><!-- .container -->
        </div>
        <div class="container"> 
            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                	<h3 class="text-center text-uppercase">Contarás con una amplia gama de posibilidades:</h3>
                </div><!-- .col-md-6 -->
            </div><!-- .row -->
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <div class="row">
                        <div class="col-md-4 margen-inferior">
                            <ul>
                            <li>Cocina abierta o cerrada (en las viviendas que sea viable)</li>
                            <li>Puertas</li>
                            <li>Armarios</li>                          
                            </ul>
                        </div><!-- .col-md-4 -->
                        <div class="col-md-4 margen-inferior">
                            <ul>
							<li>Mobiliario de baño</li>
                            <li>Solado</li>
                            <li>Domótica</li>
                            </ul>
                        </div><!-- .col-md-4 -->
                        <div class="col-md-4 margen-inferior">
                            <ul>
                            <li>Mecanismos</li>
                            <li>Cocina</li>
							<li>Pintura</li>
                            <li>Automatización de persianas</li>
                            </ul>
                        </div><!-- .col-md-4 -->
                    </div><!-- .row -->
                </div><!-- .col-md-8 -->
            </div><!-- .row -->
            <div class="row">
                <div class="col-md-12">
                    <p><img src="<?php echo $base_url; ?>/img/personalizacion/01.jpg" alt="comprar casa donostia" class="img-responsive"></p>
                </div><!-- .col-md-12 -->
            </div><!-- .row -->

            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                	<h2 class="text-center text-uppercase">Estamos muy cerca para asesorarte en todo lo que necesites</h2>
					<p>
					Puedes visitarnos en nuestro local de Bilbao ubicado en  Alameda de Urqujo, 63, 48003 Bilbao. Te atenderemos de forma personalizada y te ayudaremos a encontrar el hogar de tu vida.
					</p>
                </div><!-- .col-md-6 -->
            </div><!-- .row -->
			
        </div>
    </article>
    <?php include('../inc/pie.php'); ?>
  </body>
</html>





