<?php include('../inc/init.php'); ?>
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        
        <title>Bilbo Barria</title>
        
        <link href='https://fonts.googleapis.com/css?family=Lato:300,400|Raleway' rel='stylesheet' type='text/css'>
        <link href="<?php echo $base_url; ?>/css/bootstrap.min.css" rel="stylesheet">
        <link href="<?php echo $base_url; ?>/css/main.css" rel="stylesheet">
        
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
    <?php 
        $seccion = "viviendas";
        $subSeccion = "forma-de-pago";
        include('../inc/cabecera.php');
    ?>
    
    <article id="contenido" class="viviendas-forma-de-pago">
        <div class="container">    
            <div class="row">
                <div class="col-md-12">
                	<h2 class="text-uppercase text-center">Una gran oportunidad</h2>
                </div><!-- .col-md-12 -->
            </div><!-- .row -->
            <div class="row">
                <div class="col-md-6">
                	<p>Trabajamos cada día con el objetivo de ofrecerte las mejores oportunidades en unas condiciones inigualables</p>
					<ul>
                        <li>2 dormitorios desde 269.000€ + iva</li>
                        <li>3 dormitorios desde 307.000€ + iva</li>
                        <li>Garaje individual cerrado desde 18.000€ + iva</li>
                        <li>Trastero desde 6.000€ + iva</li>
                    </ul>
                </div><!-- .col-md-6 -->
                <div class="col-md-6">
                	<p><strong>Forma de pago:</strong>
                    <ul>
                        <li>3.000 € en el momento de hacer la reserva. </li>
                        <li>20% a la firma del contrato de compra venta.</li>
                        <li>10% al año de construcción.                </li>
                        <li>70% a la firma de la escritura.            </li>
                    </ul>
                    <p><strong>Todas las cantidades entregadas estarán garantizadas por una entidad bancaria.</strong></p>
					

                    <!--<p class="kutxabank"><strong>Avalado y financiado por Kutxabank</strong></p>-->
                </div><!-- .col-md-6 -->
            </div><!-- .row -->
        </div><!-- .container -->
    </article>
    <?php include('../inc/pie.php'); ?>
  </body>
</html>





