<?php include('inc/init.php'); ?>
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        
        <title>Bilbo Barria - Contacto</title>
        
        <link href='https://fonts.googleapis.com/css?family=Lato:300,400|Raleway' rel='stylesheet' type='text/css'>
        <link href="<?php echo $base_url; ?>/css/bootstrap.min.css" rel="stylesheet">
        <link href="<?php echo $base_url; ?>/css/main.css" rel="stylesheet">
        
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        
        <!--<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=true"></script>-->
		<script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyBcqUz9mKhRxyK4BEv1PEipkBjPb5-wXfQ"></script>
    </head>
    <body onload="initialize(43.2610978,  -2.9435795)">
		
    <?php 
        $seccion = "contacto";
        include('inc/cabecera.php');
    ?>
    
    <article id="contenido" class="contacto">
        <div class="container">
        	<div class="row">
        	    <div class="col-md-6 col-md-offset-3">
        	    	<h2 class="text-uppercase text-center">Contacto</h2>
        	    </div><!-- .col-md-6 -->
        	</div><!-- .row -->
        	
        	<div class="row">
        	    <div class="col-md-6">
        	    	<div id="map"></div>
        	    </div><!-- .col-md-6 -->
        	    <div class="col-md-6">
        	    	<p>
        	    	    Si deseas más información puedes visitarnos en nuestras oficinas ubicadas en <strong>Bilbao, Alameda de Urquijo 63</strong>.<br/>
						Nuestro equipo de profesionales te ofrecerá todo el asesoramiento necesario para que puedas encontrar la vivienda que mejor se adapte a tus necesidades. Estaremos encantados de atenderte y ofrecerte todo el asesoramiento que necesites.<bt>
						Te esperamos.
                    </p>
                    <h3>
                        Oficina comercial <br/>
						
                    </h3>
        	    	<p>
                        Alameda de Urquijo 63, 48003 Bilbao<br />
                        <a href="mailto:<?=$emailcomercial?>"><?=$emailcomercial?></a><br />
                        944.798.600<br />
                    </p>
					<h3>
					La comercialización dará inicio el 3 de marzo de 2017
					<h3>
                    <h3>
                        Horario ininterrumpido
                    </h3>
					
        	    	<p>
                        Lunes a viernes: de 8:00 a 20:00h<br />
						Sábados: de 10:00 a 14:00h
                    </p>
        	    </div><!-- .col-md-6 -->
        	</div><!-- .row -->
<!--
        	<div class="row">
        	    <div class="col-md-6 col-md-offset-3">
        	    	<h2 class="text-center">Complete el formulario para ponerse en contacto</h2>
        	    </div>
        	</div>
        	
        	<div class="row">
        	    <div class="col-md-6 col-md-offset-3">
        	    	<form id="contacto" name="contacto" method="post" action="">
        	    		<p>
        	    			<label for="nombre">Nombre y apellidos</label>
        	    			<input type="text" name="nombre" id="nombre" class="form-control" />
        	    		</p>
        	    		<p>
        	    			<label for="email">E-mail</label>
        	    			<input type="text" name="email" id="email" class="form-control"  />
        	    		</p>
        	    		<p>
        	    			<label for="telefono">Teléfono (opcional)</label>
        	    			<input type="text" name="telefono" id="telefono" class="form-control"  />
        	    		</p>
        	    		<p>
        	    			<label for="comentario">Comentario</label>
        	    			<textarea name="mensaje" id="mensaje" rows="10" cols="40" class="form-control" ></textarea>
        	    		</p>
        	    		<p>
        	    		    <input type="checkbox" name="acepto" id="acepto" value="si"> He leído y acepto la <a href="<?php echo $base_url; ?>/<?php echo $idioma; ?>/politica-de-privacidad.php" target="_blank">Política de privacidad</a>.
        	    		 </p>
        	    		<p>
        	    			<input type="submit" name="enviar" id="enviar_formulario" value="Enviar" class="btn btn-default" />
        	    		</p>
        	    	</form>
        	    	<div id="resultadoMensaje"></div>
        	    </div>
        	</div>
-->
        </div><!-- .container -->
    </article>
    <?php include('inc/pie.php'); ?>
  </body>
</html>





