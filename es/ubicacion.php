<?php include('inc/init.php'); ?>
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        
        <title>Bilbo Barria - Ubicación</title>
        
        <link href='https://fonts.googleapis.com/css?family=Lato:300,400|Raleway' rel='stylesheet' type='text/css'>
        <link href="<?php echo $base_url; ?>/css/bootstrap.min.css" rel="stylesheet">
        <link href="<?php echo $base_url; ?>/css/main.css" rel="stylesheet">
        
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        
        <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=true"></script>
    </head>
    <body onload="initialize(43.3145801,-1.9872902)">
    <?php 
        $seccion = "ubicacion";
        include('inc/cabecera.php');
    ?>
    
    <article id="contenido" class="ubicacion">
        <div class="container">    
            <!--
			<div class="row">
                <div class="col-md-12">
                    <div id="map"></div>
                </div>
            </div> 
			-->
            <div class="row">
                <div class="col-md-12">
                    <p><img src="<?php echo $base_url; ?>/img/ubicacion/viviendas.jpg" alt="viviendas-sin" class="img-responsive">
                </div><!-- .col-md-12 -->
                <div class="col-md-10 col-md-offset-1 margen-inferior">
                	<h2 class="text-center text-uppercase">En una excelente ubicación de Bilbao</h2>
                	<p class="text-center">
					La Promoción Bilbo Barria se encuentra en una ubicación privilegiada y estratégica, ya que es uno de los mejores ámbitos de Bilbao que ofrece las mejores conexiones y posibilidades de transporte
					
					</p>
                	<p class="text-center">
				La zona cuenta con nuevas y modernas infraestructuras que han sido pensadas para facilitar y agilizar tanto el acceso al centro de la ciudad como la salida a diferentes destinos. 
					</p>
                	<p class="text-center">
				Con la nueva línea de Metro L3: Etxebarri-Norte – Matiko tendrás la parada de metro Uribarri a 3 minutos a pie de tu casa y desde ahí en  2 minutos estarás en el Casco Viejo. 
					</p>
                	<p class="text-center">
				En 4 minutos llegarás a pie al Ayuntamiento y además, la zona cuenta con la línea A1 de autobús con la que estarás en 5 minutos en Gran Vía.
					</p>
                </div><!-- .col-md-8 -->
            </div><!-- .row -->

				
			
            <di
        </div>
    </article>
    <?php include('inc/pie.php'); ?>
  </body>
</html>





