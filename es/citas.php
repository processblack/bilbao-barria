<?php include('inc/init.php'); ?>
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        
        <title>Bilbo Barria - Citas</title>
        
        <link href='https://fonts.googleapis.com/css?family=Lato:300,400|Raleway' rel='stylesheet' type='text/css'>
        <link href="<?php echo $base_url; ?>/css/bootstrap.min.css" rel="stylesheet">
        <link href="<?php echo $base_url; ?>/css/main.css" rel="stylesheet">
        
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        
    </head>
    <body>
    <?php 
        $seccion = "citas";
        include('inc/cabecera.php');
    ?>
    
    <article id="contenido" class="constructora">
        <div class="container">
        	<div class="row">
        	    <div class="col-md-6 col-md-offset-3">
        	    	<h2 class="text-uppercase text-center">Reserva tu cita</h2>
        	    </div><!-- .col-md-6 -->
        	</div><!-- .row -->
        	
        	<div class="row">
        	    <div class="col-md-6">
        	    	<p>Reserva una cita en nuestro <a href="<?php echo $base_url; ?>/<?php echo $idioma; ?>/contacto.php">local comercial de Bilbao ubicado en la Alameda de Urquijo 63</a>, y te ayudaremos en la elección del hogar de tu vida.</p>
					<p><strong>Si no ves correctamente la reserva de citas pulsa a continuación</strong>: <a href="https://app.bookitit.com/es/widgets/dtlbook/2ea085690fb6c41a61fad8e4c1f3c0f68/2/d_access/1/bkt204985/">Reserva de Citas</a></p>
                    
        	    </div><!-- .col-md-6 -->
				<div class="col-md-6">
        	    	
					<p>		
						<script type="text/javascript"> var sHref = document.location.href; var sName = parent.location.href; var iIndex = sHref.indexOf("?"); var sParams = ""; if (iIndex >= 0) { sHref = sHref.substring(iIndex,sHref.length); } else { sHref = ""; } var sSrc = "https://app.bookitit.com/es/widgets/dtlbook/2ea085690fb6c41a61fad8e4c1f3c0f68/2/d_access/1/bkt204985/"; document.write('<iframe src="'+sSrc+'" width="585" height="900" scrolling="no" frameborder="0" name="'+sName+'"> </iframe>'); </script>
					</p>

        	    </div><!-- .col-md-6 -->
        	    				
        	</div><!-- .row -->

        </div><!-- .container -->
        
    </article>
    <?php include('inc/pie.php'); ?>
  </body>
</html>





