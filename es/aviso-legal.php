<?php include('inc/init.php'); ?>
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        
        <title>Aviso Legal</title>
        
        <link href='https://fonts.googleapis.com/css?family=Lato:300,400|Raleway' rel='stylesheet' type='text/css'>
        <link href="<?php echo $base_url; ?>/css/bootstrap.min.css" rel="stylesheet">
        <link href="<?php echo $base_url; ?>/css/main.css" rel="stylesheet">
        
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        
    </head>
    <body>
    <?php 
        $seccion = "aviso-legal";
        include('inc/cabecera.php');
    ?>
    
    <article id="contenido" class="legal">
        <div class="container">
        	<div class="row">
        	    <div class="col-md-6 col-md-offset-3">
        	    	<h2 class="text-uppercase text-center">Aviso Legal</h2>
        	    </div><!-- .col-md-6 -->
        	</div><!-- .row -->
        	
        	<div class="row">
        	    <div class="col-md-12">
        	    	<h3>1. Identidad del titular de la presente web</h3>
                    <p>En cumplimiento de la Ley 34/2002, de 11 de julio, de Servicios de la Sociedad de la Información y de Comercio Electrónico, le informamos que el titular de la página web de www.beriogoienetxe.com es:<br />
                    <strong>Razón social:</strong><br />
                    AMENABAR PROMOCIONES RESIDENCIALES, SL<br />
                    <strong>C.I.F.:</strong><br />
                    B-86843943<br />
                    <strong>Dirección:</strong><br />
                    Calle Nafarroa 50, 20800 Zarautz, Guipúzcoa<br />
                    <strong>Teléfono:</strong><br />
                    943 831100<br />
                    <strong>Fax:</strong><br />
                    943 890124<br />
                    <strong>Correo electrónico:</strong><br />
                    <a href="mailto:info@amenabarpromociones.com">info@amenabarpromociones.com</a><br />
                    <strong>Datos registrales:</strong><br />
                    Inscrita en el Registro Mercantil Guipúzcoa 5 de Mayo 1982, Folio 148, Hoja num. 6986, Tomo 488 de Sociedades, Inscripción 1a.
                    </p>
                    
                    <h3>2. Condiciones generales de uso de este sitio web</h3>
                    
                    <p>El acceso a esta página web le atribuye la condición de “Usuario”, implicando su aceptación expresa y sin reservas de las condiciones generales que estén publicadas en www.beriogoienetxe.com, en el momento en que acceda a la web. Por lo tanto, el Usuario deberá leer las presentes condiciones generales de uso, en cada una de las ocasiones en que se proponga acceder y en su caso utilizar los servicios prestados a través de esta web, ya que éstas pueden sufrir modificaciones.</p>
                    <p>En este sentido, se entenderá por “Usuario” a la persona que acceda, navegue, utilice o participe en los servicios y actividades, gratuitas u onerosas, desarrolladas en www.beriogoienetxe.com.</p>
                    
                    <h4>2.1. Objeto y ámbito de aplicación</h4>
                    
                    <p>Las presentes condiciones generales de uso, regulan el acceso, navegación y uso de www.barandiaranberri.com así como las responsabilidades derivadas de la utilización de sus contenidos tales como los textos, gráficos, dibujos, diseños, códigos, software, fotografías, música, vídeos, sonidos, bases de datos, imágenes, expresiones e informaciones, así como cualquier otra creación protegida por las leyes nacionales y los tratados internacionales sobre propiedad intelectual e industrial.</p>
                    <p>AMENABAR PROMOCIONES RESIDENCIALES, SL podrá establecer condiciones particulares que serán de aplicación para el uso y/o contratación de servicios específicos siendo las presentes condiciones generales de aplicación supletoria.</p>
                    
                    <h4>2.2. Condiciones acceso y utilización de los servicios</h4>
                    
                    <p>El acceso a www.barandiaranberri.com en principio tiene carácter libre y gratuito. Los menores de que pretendan hacer uso de los servicios contenidos en este sitio web deberán contar con el consentimiento previo de sus padres, tutores o representantes legales, siendo éstos los únicos responsables de los actos realizados por los menores a su cargo. En todo momento, el Usuario debe realizar un uso lícito de los servicios de la presente web, de acuerdo con las presentes condiciones generales, la legalidad vigente, la moral y el orden público así como las prácticas generalmente aceptadas en Internet. El Usuario garantiza que toda la información aportada a través de los formularios de suscripción incluidos en esta web es lícita, real, exacta, veraz y actualizada. Será exclusiva responsabilidad del Usuario la comunicación inmediata a AMENABAR PROMOCIONES RESIDENCIALES, SL de cualquier modificación que pueda darse en la información suministrada. El Usuario se abstendrá de: introducir virus, programas, macros o cualquier secuencia de caracteres con la finalidad de dañar o alterar los sistemas informáticos de esta web; obstaculizar el acceso de otros usuarios mediante el consumo masivo de recursos; captar datos incluidos en esta web con finalidad publicitaria; reproducir, copiar, distribuir, transformar o poner a disposición de terceros los contenidos incluidos en esta web; realizar acciones a través de los servicios incluidos es esta web que puedan lesionar la propiedad intelectual, secretos industriales, compromisos contractuales, derechos al honor, a la imagen y la intimidad personal de terceros; realizar acciones de competencia desleal y publicidad ilícita.</p>
                    
                    <h3>3. Derechos de propiedad intelectual e industrial</h3>
                    
                    <p>AMENABAR PROMOCIONES RESIDENCIALES, SL es titular de los derechos de propiedad intelectual e industrial de todos los elementos que integran www.barandiaranberri.com, incluyendo la marca, nombre comercial o signo distintivo. En particular y a título no limitativo, están protegidos por los derechos de autor, el diseño gráfico, código fuente, logos, textos, gráficos, ilustraciones, fotografías, sonidos y demás elementos contenidos en sitio web.</p>
                    <p>En ningún caso el acceso o navegación en www.barandiaranberri.com, implica renuncia, transmisión o licencia total o parcial de AMENABAR PROMOCIONES RESIDENCIALES, SL para uso personal al Usuario sobre sus derechos de propiedad intelectual e industrial.</p>
                    <p>Por ello, el Usuario reconoce que la reproducción, copiado, distribución, comercialización, transformación, reutilización, comunicación pública y en general, cualquier otra forma de explotación, por cualquier procedimiento, de todo o parte de los contenidos de www.barandiaranberri.com, sin autorización expresa y por escrito de AMENABAR PROMOCIONES RESIDENCIALES, SL, constituye una infracción de sus derechos de propiedad intelectual y/o industrial.</p>
                    <p>Toda la información que no tenga la consideración de dato personal transmitida voluntariamente a www.barandiaranberri.com a través de Internet (incluyendo cualquier observación, sugerencia, idea, gráficos, etc.) pasará a ser propiedad exclusiva de AMENABAR PROMOCIONES RESIDENCIALES, SL que ostentará los derechos ilimitados de uso, sin devengar por ello ninguna compensación a su favor, ni a favor de ninguna otra persona.</p>
                    
                    <h3>4. Régimen de responsabilidades y garantías</h3>
                    
                    <p>AMENABAR PROMOCIONES RESIDENCIALES, SL no garantiza la licitud, fiabilidad, utilidad, veracidad o exactitud de los servicios o de la información incluida en www.barandiaranberri.com, por tanto excluye cualquier responsabilidad directa o indirecta por daños y perjuicios de toda naturaleza, derivada de la defraudación de utilidad o expectativas que el Usuario haya depositado en la misma. AMENABAR PROMOCIONES RESIDENCIALES, SL declara que ha adoptado las medidas tanto técnicas como organizativas, que dentro de sus posibilidades y el estado de la tecnología, permitan el correcto funcionamiento de la web así como la ausencia de virus y componentes dañinos, sin embargo no puede hacerse responsable de: (a) la continuidad y disponibilidad de los contenidos y servicios recogidos en www.barandiaranberri.com; (b) la ausencia de errores en dichos contenidos ni la corrección de cualquier defecto que pudiera ocurrir; (c) la ausencia de virus y/o demás componentes dañinos en www.barandiaranberri.com; (d) la inexpugnabilidad de las medidas de seguridad que se han adoptado; (e) los daños o perjuicios que cause cualquier persona que vulnere los sistemas de seguridad de www.barandiaranberri.com. El Usuario será el único responsable ante terceros, de cualquier comunicación enviada personalmente o a su nombre a www.barandiaranberri.com, así como del uso ilegítimo de los contenidos y servicios contenidos en esta web. AMENABAR PROMOCIONES RESIDENCIALES, SL se reserva el derecho a suspender temporalmente y sin previo aviso, la accesibilidad a www.barandiaranberri.com con motivo de operaciones de mantenimiento, reparación, actualización o mejora. Siempre que las circunstancias lo permitan AMENABAR PROMOCIONES RESIDENCIALES, SL publicará en su web, con antelación suficiente, un aviso indicando la fecha prevista para la suspensión de los servicios. Los enlaces a otras páginas web que en su caso existan en www.barandiaranberri.com pueden llevarle a sitios web de los que AMENABAR PROMOCIONES RESIDENCIALES, SL no asume ninguna responsabilidad, ya que no tiene ningún tipo de control sobre los mismos, siendo su finalidad, informar al Usuario de otras fuentes de información, por lo que el Usuario accede bajo su exclusiva responsabilidad al contenido y en las condiciones de uso que rijan en los mismos. AMENABAR PROMOCIONES RESIDENCIALES, SL no se responsabiliza del uso que los Usuarios puedan hacer de los contenidos y servicios incluidos en su web. En consecuencia no garantiza que el uso que los Usuarios puedan hacer de los contenidos y servicios referidos, se ajusten a las presentes condiciones generales de uso, ni que lo hagan de forma diligente.</p>
                    
                    <h3>5. Duración y modificación</h3>
                    
                    <p>AMENABAR PROMOCIONES RESIDENCIALES, SL se reserva el derecho a modificar, total o parcialmente, las presentes condiciones generales de acceso publicando los cambios en www.barandiaranberri.com. Asimismo podrá efectuar sin previo aviso las modificaciones que considere oportunas en el sitio web, pudiendo cambiar, suprimir o añadir tanto los contenidos y servicios que presta, como la forma en la que éstos aparezcan presentados o localizados. En consecuencia se entenderán como vigentes, las condiciones generales que estén publicadas en el momento en el que el Usuario acceda a www.barandiaranberri.com por lo que el Usuario deberá leer periódicamente dichas condiciones de uso. Con independencia de lo dispuesto en las condiciones particulares, AMENABAR PROMOCIONES RESIDENCIALES, SL podrá dar por terminado, suspender o interrumpir, en cualquier momento sin necesidad de preaviso, el acceso a los contenidos de la página, sin posibilidad por parte del Usuario de exigir indemnización alguna.</p>
                    
                    <h3>6. Legislación aplicable y competencia jurisdiccional</h3>
                    
                    <p>Las relaciones establecidas entre AMENABAR PROMOCIONES RESIDENCIALES, SL y el Usuario se regirán por lo dispuesto en la normativa vigente acerca de la legislación aplicable y la jurisdicción competente. No obstante, para los casos en los que la normativa prevea la posibilidad a las partes de someterse a un fuero, AMENABAR PROMOCIONES RESIDENCIALES, SL y el Usuario, con renuncia expresa a cualquier otro fuero que pudiera corresponderles, se someten a los Juzgados y Tribunales de la ciudad de San Sebastián.</p>
                    
                    <h3>7. Protección de datos de carácter personal</h3>
                    
                    <p>En cumplimiento de la Ley Orgánica 15/1999, de 13 de diciembre, de Protección de Datos de Carácter Personal (LOPD), le informamos que los datos personales que nos suministre a través de la página web www.barandiaranberri.com, serán tratados de forma confidencial y pasarán a formar parte de un fichero automatizado titularidad de AMENABAR PROMOCIONES RESIDENCIALES, SL, dirección calle Nafarroa 50, Zarautz, Guipúzcoa, que ha sido debidamente inscrito en la Agencia Española de Protección de Datos (www.agpd.es).</p>
                    
                    <p>Sus datos personales serán utilizados con la finalidad que se indique en las páginas donde se encuentre el formulario electrónico de recogida de datos personales. Asimismo, podrán ser conservados para contestar a su solicitud y para el envío (por canales ordinarios o electrónicos) de información relacionada con nuestra actividad que pueda resultar de su interés, hasta que nos indique lo contrario. Asimismo le informamos que puede ejercitar sus derechos de acceso, rectificación, cancelación y oposición con arreglo a lo previsto en la LOPD, enviando una carta certificada o forma equivalente que permita acreditar su recepción, junto con la fotocopia de su D.N.I., a la siguiente dirección: calle Nafarroa 50, 20800, Zarautz, Guipúzcoa.</p>
                    
                    <p>La no cumplimentación de los campos, indicados como obligatorios, que aparecen en cualquier formulario de registro electrónico, podrá tener como consecuencia que AMENABAR PROMOCIONES RESIDENCIALES, SL no pueda atender a su solicitud.</p>
                    
                    <p>AMENABAR PROMOCIONES RESIDENCIALES, SL no vende, cede, arrienda ni transmite de ningún modo, información o datos de carácter personal de sus Clientes/Usuarios a terceros.</p>
                    <p>A los efectos de lo que se indica en el párrafo anterior, le informamos de que AMENABAR PROMOCIONES RESIDENCIALES, SL tiene instaladas cookies en sus sistemas.</p>
                    
                    <p>Las cookies son pequeños archivos de texto que almacena el navegador en el disco duro de su ordenador. Cuando navega a través de nuestra página web, nuestro servidor podrá reconocer la cookie y proporcionarnos información sobre su última visita. La mayoría de los navegadores aceptan la utilización de cookies de forma automática, pero puede configurar su navegador para ser avisado en su pantalla de ordenador de la recepción de cookies y poder impedir su instalación en su disco duro.</p>
                    
                    <p>El Usuario garantiza que los Datos Personales facilitados a AMENABAR PROMOCIONES RESIDENCIALES, SL son veraces y se hace responsable de comunicar cualquier modificación en los mismos. El Usuario será el único responsable de cualquier daño o perjuicio, directo o indirecto, que pudiera ocasionar a AMENABAR PROMOCIONES RESIDENCIALES, SL o a cualquier tercero a causa de la cumplimentación de los formularios con datos falsos, inexactos, incompletos o no actualizados.</p>
                    
                    <p>El Usuario no deberá incluir datos personales de terceros sin su consentimiento informado previo de lo establecido en la presente política de privacidad, siendo el único responsable de su inclusión.</p>
                    
                    <p>Ciertos servicios prestados por AMENABAR PROMOCIONES RESIDENCIALES, SL, pueden contener condiciones particulares con previsiones específicas en materia de protección de datos personales, siendo por tanto de aplicación las condiciones particulares de cada caso. En todo lo no regulado por las condiciones particulares será de aplicación supletoria las condiciones generales recogidas en la presente política de privacidad.</p>
                    
                    <p>Se advierte a los niños y jóvenes menores de 18 años que han de tener el permiso de sus padres o tutores para proporcionar datos personales en el sitio web de AMENABAR PROMOCIONES RESIDENCIALES, SL</p>
                    
                    <p>AMENABAR PROMOCIONES RESIDENCIALES, SL ha adoptado las medidas necesarias para evitar la alteración, pérdida, tratamiento o acceso no autorizado de los datos personales, habida cuenta en todo momento del estado de la tecnología, la naturaleza de los datos almacenados y los riesgos a que están expuestos, ya provengan de la acción humana o del medio físico o natural, no obstante, el Usuario debe ser consciente de que las medidas de seguridad en Internet no son inexpugnables.</p>
                    
                    <p>AMENABAR PROMOCIONES RESIDENCIALES, SL se reserva el derecho de modificar su política de privacidad de acuerdo a su criterio, o a causa de un cambio legislativo, jurisprudencial o en la práctica empresarial. Si AMENABAR PROMOCIONES RESIDENCIALES, SL introdujera alguna modificación, el nuevo texto será publicado en este mismo sitio web, donde el Usuario podrá tener conocimiento de la política de privacidad actual de AMENABAR PROMOCIONES RESIDENCIALES, SL En cualquier caso, la relación con los usuarios se regirá por las normas previstas en el momento preciso en que se accede al sitio web.</p>
                    
                    <p>La cumplimentación de los formularios incluidos en el sitio web, implica el consentimiento expreso del Usuario a la inclusión de sus datos de carácter personal en el referido fichero automatizado de AMENABAR PROMOCIONES RESIDENCIALES, SL con las finalidades indicadas.</p>
                    
                    <h3>8. Política de uso de cookies</h3>
                    
                    <p>Cookie es un fichero que se descarga en su ordenador al acceder a determinadas páginas web. Las cookies permiten a una página web, entre otras cosas, almacenar y recuperar información sobre los hábitos de navegación de un usuario o de su equipo y, dependiendo de la información que contengan y de la forma en que utilice su equipo, pueden utilizarse para reconocer al usuario. El navegador del usuario memoriza cookies en el disco duro solamente durante la sesión actual ocupando un espacio de memoria mínimo y no perjudicando al ordenador. Las cookies no contienen ninguna clase de información personal específica, y la mayoría de las mismas se borran del disco duro al finalizar la sesión de navegador (las denominadas cookies de sesión).</p>
                    <p>La mayoría de los navegadores aceptan como estándar a las cookies y, con independencia de las mismas, permiten o impiden en los ajustes de seguridad las cookies temporales o memorizadas.</p>
                    <p>Sin su expreso consentimiento –mediante la activación de las cookies en su navegador–XXXXX no enlazará en las cookies los datos memorizados con sus datos personales proporcionados en el momento del registro o la compra.</p>
                    
                    
                    
                    
                    <h4>¿Qué tipos de cookies utiliza esta página web?</h4>
                    
                    <p>- Cookies técnicas: Son aquéllas que permiten al usuario la navegación a través de una página web, plataforma o aplicación y la utilización de las diferentes opciones o servicios que en ella existan como, por ejemplo, controlar el tráfico y la comunicación de datos, identificar la sesión, acceder a partes de acceso restringido, recordar los elementos que integran un pedido, realizar el proceso de compra de un pedido, realizar la solicitud de inscripción o participación en un evento, utilizar elementos de seguridad durante la navegación, almacenar contenidos para la difusión de vídeos o sonido o compartir contenidos a través de redes sociales.</p>
                    
                    <p>- Cookies de análisis: Son aquéllas que bien tratadas por nosotros o por terceros, nos permiten cuantificar el número de usuarios y así realizar la medición y análisis estadístico de la utilización que hacen los usuarios del servicio ofertado. Para ello se analiza su navegación en nuestra página web con el fin de mejorar la oferta de productos o servicios que le ofrecemos.</p>
                    
                    <p>Cookies de terceros: El presente sitio web puede utilizar servicios de terceros que recopilaran información con fines estadísticos, de uso del Site por parte del usuario y para la prestación de otros servicios relacionados con la actividad del Website y otros servicios de Internet.</p>
                    
                    <p>En particular, este sitio Web utiliza Google Analytics, un servicio analítico de web prestado por Google, Inc. con domicilio en los Estados Unidos con sede central en 1600 Amphitheatre Parkway, Mountain View, California 94043. Para la prestación de estos servicios, estos utilizan cookies que recopilan la información, incluida la dirección IP del usuario, que será transmitida, tratada y almacenada por Google en los términos fijados en la Web Google.com. Incluyendo la posible transmisión de dicha información a terceros por razones de exigencia legal o cuando dichos terceros procesen la información por cuenta de Google.</p>
        	    </div><!-- .col-md-6 -->
        	</div><!-- .row -->
        </div><!-- .container -->
    </article>
    <?php include('inc/pie.php'); ?>
  </body>
</html>





