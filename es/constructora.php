<?php include('inc/init.php'); ?>
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        
        <title>Bilbo Barria - La constructora</title>
        
        <link href='https://fonts.googleapis.com/css?family=Lato:300,400|Raleway' rel='stylesheet' type='text/css'>
        <link href="<?php echo $base_url; ?>/css/bootstrap.min.css" rel="stylesheet">
        <link href="<?php echo $base_url; ?>/css/main.css" rel="stylesheet">
        
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        
    </head>
    <body>
    <?php 
        $seccion = "constructora";
        include('inc/cabecera.php');
    ?>
    
    <article id="contenido" class="constructora">
        <div class="container">
        	<div class="row">
        	    <div class="col-md-6 col-md-offset-3">
        	    	<h2 class="text-uppercase text-center">Con la garantía de Construcciones Amenabar.</h2>
        	    </div><!-- .col-md-6 -->
        	</div><!-- .row -->
        	
        	<div class="row">
        	    <div class="col-md-6">
        	    	<p><img src="<?php echo $base_url; ?>/img/constructora/seguridad.jpg" alt="seguridad" class="img-responsive"></p>
        	    </div><!-- .col-md-6 -->
        	    <div class="col-md-6">
        	    	<p>En Construcciones Amenabar ofrecemos cada vivienda con todas las garantías y el respaldo de una empresa con más de 30 años de experiencia en el sector. Experiencia en obra civil, en construcción de viviendas y en construcción de obras singulares.</p>
                    <p>Durante nuestra trayectoria hemos afrontado con éxito la construcción de más de 10.000 viviendas. Además contamos con una amplia experiencia en Viviendas Protegidas.</p>
                    <p>Nuestro trabajo nos convierte en un referente, con capacidad y solvencia para llevar a cabo los proyectos más ambiciosos y significativos, y de ejecutar todo tipo de obras, tanto públicas como privadas.</p>
                    <p>Con compromiso, con excelencia y con la total satisfacción de nuestros numerosos clientes. </p>
                    <p>Puedes conocernos mejor en:</p>
                    <p><a href="http://www.construccionesamenabar.com">www.construccionesamenabar.com</a></p>
        	    </div><!-- .col-md-6 -->
        	</div><!-- .row -->

        </div><!-- .container -->
        <div class="fondo-gris">
            <div class="container">
            	<div class="row">
            	    <div class="col-md-6 col-md-offset-3">
            	    	<h2 class="text-uppercase text-center">Una amplia experiencia a su servicio.</h2>
            	    </div><!-- .col-md-6 -->
            	</div><!-- .row -->
            	<div class="row">
            	    <div class="col-md-10 col-md-offset-1 margen-inferior"">
            	    	<p class="text-center">Nuestras principales zonas de actuación son: Comunidad Autónoma de Euskadi, Navarra, La Rioja, Madrid, Málaga, Cantabria y Mallorca.</p>
            	    	<p class="text-center">Hemos desarrollado todo tipo de obras tanto públicas como de carácter privado: Edificación, Urbanizaciones, construcción de Polígonos Industriales, Zonas Deportivas, Restauración de Edificios y Monumentos así como edificios destinados a viviendas y Obra Civil.</p>
            	    </div><!-- .col-md-10 -->
            	    <div class="col-md-12">
            	    	<img src="../img/constructora/ofi_amenabar.jpg" alt="ofi_amenabar" class="img-responsive">
            	    </div><!-- .col-md-12 -->
            	</div><!-- .row -->
            </div><!-- .container -->
        </div>
    </article>
    <?php include('inc/pie.php'); ?>
  </body>
</html>





