<?php include('inc/init.php'); ?>
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
		
		<meta name="google-site-verification" content="XDSJ4niooZKsaqUb6s9ZieD97FGgxaEdgmND-FgGm9Y" />
        
        <title>Bilbo Barria</title>
        
        <link href='https://fonts.googleapis.com/css?family=Lato:300,400|Raleway' rel='stylesheet' type='text/css'>
        <link href="<?php echo $base_url; ?>/css/bootstrap.min.css" rel="stylesheet">
        <link href="<?php echo $base_url; ?>/css/main.css" rel="stylesheet">
        
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body class="home">
    <?php 
        $seccion = "index";
        include('inc/cabecera.php');
    ?>
    
    <article id="contenido" class="home">
        <div id="slide-home" class="cycle-slideshow" 
            data-cycle-fx="fade" 
            data-cycle-timeout="4000"
            data-cycle-slides=".slides > div"
            data-cycle-log="false"
            data-cycle-pause-on-hover="true"
            data-cycle-pager="#bullets"
            data-cycle-pager-template="<strong><a href=#><span>{{slideNum}}<span></a> </strong>">
            <div class="slides">
                <div class="slide" style="background: url(../img/home/piscina-home.jpg) no-repeat center bottom; background-size: cover; ">
                    <div class="texto">96 viviendas libres en una excelente ubicación en Bilbao</div>
                </div>
                <div class="slide" style="background: url(<?php echo $base_url; ?>/img/home/salon-home.jpg) no-repeat  center top; background-size: cover;">
                    <div class="texto">2 dormitorios desde 269.000 euros <small>+ iva</small> </div>
                </div>
                <div class="slide" style="background: url(<?php echo $base_url; ?>/img/home/gimnasio-home.jpg) no-repeat 50%; background-size: cover;">
                    <div class="texto">3 dormitorios desde 307.000 euros <small>+ iva</small></div>
                </div>
            </div><!-- .slide -->
            <div id="bullets"></div>
        </div><!-- #slide-home -->
            

        
      
    </article>
    <?php include('inc/pie.php'); ?>
 </body>
 </html>