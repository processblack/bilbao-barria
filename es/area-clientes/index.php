<?php include('../inc/init.php'); ?>
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        
        <title>Miramón Berri - Área de clientes</title>
        
        <link href='https://fonts.googleapis.com/css?family=Lato:300,400|Raleway' rel='stylesheet' type='text/css'>
        <link href="<?php echo $base_url; ?>/css/bootstrap.min.css" rel="stylesheet">
        <link href="<?php echo $base_url; ?>/css/main.css" rel="stylesheet">
        
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        
    </head>
    <body>
    <?php include('../inc/cabecera.php'); ?>
    
    <article id="contenido" class="area-clientes">
    <div class="container">
    	<div class="row">
    	    <div class="col-md-12">
    	    	<h2 class="text-uppercase text-center">Aquí puedes personalizar tu vivienda.</h2>
    	    </div><!-- .col-md-12 -->
    	</div><!-- .row -->
    	<div class="row">
    	    <div class="col-md-6">
    	    	<p>1- Si ya has formalizado la compra de tu vivienda y quieres 
que te enviemos tu contraseña pincha en el enlace “Solicita o recupera tu contraseña”. </p>
                <p>2- Te enviaremos tu contraseña a la dirección de correo electrónico que nos consta en nuestro sistema.</p>
    	    </div><!-- .col-md-6 -->
    	    <div class="col-md-6">
    	    	<p>3- Entra nuevamente en www.miramonberri.com, en el apartado “Área Clientes” e ingresa tu DNI y la contraseña que te hemos enviado.</p>
    	    	<p>4- Ya puedes comenzar a personalizar tu casa a tu gusto.</p>
    	    </div><!-- .col-md-6 -->
    	</div><!-- .row -->
    	<div class="row">
    	    <div class="col-md-8 col-md-offset-2">
    	        <form id="login" name="login" method="post" action="index-2.php" class="fondo-gris" style="padding: 20px; margin: 20px 0; border: 1px solid #aaa;">
    	            <h3 class="text-center text-uppercase">Ingresa tu NIF y contraseña</h3>
    	        	<p class="form-group">
    	        		<label for="nombre">NIF (sin espacios, ni puntos, ni guiones)</label>
    	        		<input type="text" name="nombre" id="nombre" class="form-control" />
    	        	</p>
    	        	<p class="form-group">
    	        		<label for="password">Contraseña</label>
    	        		<input type="password" name="password" id="password" class="form-control" />
    	        	</p>
    	        	<p class="form-group">
    	        		<input type="submit" name="enviar" id="enviar" value="Entrar"  class="btn btn-default" />
    	        	</p>
    	        	<p><a href="#">Solicita o recupera tu contraseña</a></p>
    	        </form>  	
    	        <p class="text-right">Atención al Cliente: <a href="mailto:info@miramon.com">info@miramon.com</a> | <a href="#">Condiciones de contratación</a></p>
    	    </div><!-- .col-md-6 -->
    	</div><!-- .row -->
    </div><!-- .container -->

    </article>
    <?php include('../inc/pie.php'); ?>
  </body>
</html>





