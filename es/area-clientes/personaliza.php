<?php include('../inc/init.php'); ?>
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        
        <title>Miramón Berri - Área de clientes</title>
        
        <link href='https://fonts.googleapis.com/css?family=Lato:300,400|Raleway' rel='stylesheet' type='text/css'>
        <link href="<?php echo $base_url; ?>/css/bootstrap.min.css" rel="stylesheet">
        <link href="<?php echo $base_url; ?>/css/main.css" rel="stylesheet">
        
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        
    </head>
    <body>
    <?php include('../inc/cabecera-clientes.php'); ?>
    
    <article id="contenido" class="area-clientes">
        <div class="container">
            <div class="titular-seccion clearfix">
                <div class="row">
                    <div class="col-md-10 col-md-offset-1">
                        <h2 class="text-uppercase text-center">Personaliza tu vivienda</h2>
                    </div>
                </div><!-- .row -->
            </div><!-- .titular-seccion -->
                    
        <form id="personalizacion" name="form1" method="post" action="">
            <section id="pasos">
                <div class="row">
                    <div class="col-md-10 col-md-offset-1">
                        <ul class="clearfix raleway">
                            <li class="current separador-azul">1. Elige</li>
                            <li class="fondo-gris">2. Revisa y confirma</li>
                        </ul>
                    </div>
                </div><!-- .row -->
            </section>  
            
            
            <section id="divisiones_interiores">
                <div class="row">
                    <div class="col-md-1">
                        <h3 class="text-right transparente">1</h3>
                    </div>
                    <div class="col-md-10">
                        <h3>Divisiones interiores</h3>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3 col-md-offset-1">
                        <h4>1.1. Cambios en tabiquería (sin modificar cuartos húmedos)</h4>
                    </div>
                    <div class="col-md-5">
                        <p>
                        <input id="solicitar" type="checkbox" name="solicitar" value="solicitar">
                        <label for="solicitar">Solicitar reunión con el departamento de técnico</label>
                        </p>                    
                    </div>
                    <div class="col-md-2 totales">
                        <p class="total text-right">Presupuesto</p>
                    </div>
                </div><!-- .row -->
            </section>
    
            <section id="carpinteria_interior">
                <div class="row">
                    <div class="col-md-1">
                        <h3 class="text-right transparente">2</h3>
                    </div>
                    <div class="col-md-10">
                        <h3>Carpintería interior</h3>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-md-3 col-md-offset-1">
                        <h4>2.1. Puertas y zócalos</h4>
                    </div>
                    <div class="col-md-5 linea_baja">
                        <dl class="clearfix">
                            <dt>
                                <input type="radio" id="puertas_1" name="puertas[]" value="roble" selected="selected">
                                <label for="puertas_1">Roble</label>
                            </dt>
                            <dd>Incluido</dd>

                            <dt>
                                <input type="radio" id="puertas_2" name="puertas[]" value="lacado">
                                <label for="puertas_2">Lacado en blanco</label>
                                <a href="<?php echo $base_url; ?>/img/area-clientes/test.jpg" class="boxer"><img src="<?php echo $base_url; ?>/img/area-clientes/ojo.png" alt="Foto"></a>
                            </dt>
                            <dd>450.00€</dd>
                        </dl>
                    </div>
                    <div class="col-md-2 totales">
                        <p class="total text-right">
                            0
                        </p>
                    </div>
                </div><!-- .row -->
    
    
                <div class="row">
                    <div class="col-md-3 col-md-offset-1">
                        <h4>2.2. Puertas correderas</h4>
                    </div>
                    <div class="col-md-5 linea_baja">
                        <dl class="clearfix">
                            <dt>
                                <input type="checkbox" id="puertas_correderas_1" name="puertas_correderas[]" value="Baño 1">
                                <label for="puertas_correderas_1">Baño 1</label>
                            </dt>
                            <dd>450.00€</dd>

                            <dt>
                                <input type="checkbox" id="puertas_correderas_2" name="puertas_correderas[]" value="Baño 2">
                                <label for="puertas_correderas_2">Baño 2</label>
                            </dt>
                            <dd>450.00€</dd>

                            <dt>
                                <input type="checkbox" id="puertas_correderas_3" name="puertas_correderas[]" value="cocina">
                                <label for="puertas_correderas_3">Cocina</label>
                            </dt>
                            <dd>450.00€</dd>
                        </dl>
                    </div>
                    <div class="col-md-2 totales">
                        <p id="puertas_total" class="total text-right">0,00 €</p>
                        <p id="puertas_total" class="total text-right">0.00 €</p>
                        <p id="puertas_total" class="total text-right">0.00 €</p>
                    </div>
                </div><!-- .row -->
    
                <div class="row">
                    <div class="col-md-3 col-md-offset-1">
                        <h4>2.4 Distribución interior armarios</h4>
                    </div>
                    <div class="col-md-5 linea_baja">
                        <p>
                            Dormitorio 1 <a href="#dormitorio1" class="boxer push"><img src="<?php echo $base_url; ?>/img/area-clientes/ojo.png" alt="Foto"></a> 
                        </p>
                        
                        <!-- capa oculta -->
                        <div id="dormitorio1" style="display: none;">
                            <div class="modal clearfix" style="width: 400px; height: 300px"><!-- Estilos en línea para fijar ancho y alto de la ventana -->
                                <!-- 
                                En el interior dos capas, izquierda y derecha
                                Poner img-max a la foto interior para que coja el 48% 
                                del ancho de la capa izquierda.
                                -->
                                <div class="izquierda">
                                    <img src="img/foto-prueba.jpg" alt="foto-prueba" class="img-max">
                                </div>
                                <div class="derecha">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam...</p>
                                    <p><a class="boton-azul" href="#">Agregar</a></p>
                                </div>
                            </div>
                        </div>
                        
                        <dl class="clearfix">
                            <dt>
                                <input type="radio" id="distribucion_armarios_1" name="distribucion_armarios[]" value="Opción 1">
                                <label for="distribucion_armarios_1">Opción 1</label>
                            </dt>
                            <dd>450.00€</dd>
                            <dt>
                                <input type="radio" id="distribucion_armarios_2" name="distribucion_armarios[]" value="Opción 2">
                                <label for="distribucion_armarios_2">Opción 2</label>
                            </dt>
                            <dd>450.00€</dd>
                            <dt>
                                <input type="radio" id="distribucion_armarios_3" name="distribucion_armarios[]" value="Opción 3">
                                <label for="distribucion_armarios_3">Opción 3</label>
                            </dt>
                            <dd>450.00€</dd>
                            <dt>
                                <input type="radio" id="distribucion_armarios_4" name="distribucion_armarios[]" value="Opción 4">
                                <label for="distribucion_armarios_4">Opción 4</label>
                            </dt>
                            <dd>450.00€</dd>
                        </dl>
                    </div>
                    <div class="col-md-2 totales">
                        <p id="puertas_total" class="total text-right">0.00 €</p>
                    </div>
                </div><!-- .row -->
                        
                <div class="row">
                    <div class="col-md-5 col-md-offset-4 linea_baja">
                        <p>
                            Dormitorio 2 <a href="#" class="boxer push"><img src="<?php echo $base_url; ?>/img/area-clientes/ojo.png" alt="Foto"></a>
                        </p>
                        <dl class="clearfix">
                            <dt>
                                <input type="radio" id="distribucion_armarios_5" name="distribucion_armarios_2[]" value="Opción 1">
                                <label for="distribucion_armarios_5">Opción 1</label>
                            </dt>
                            <dd>450.00€</dd>
                            <dt>
                                <input type="radio" id="distribucion_armarios_6" name="distribucion_armarios_2[]" value="Opción 2">
                                <label for="distribucion_armarios_6">Opción 2</label>
                            </dt>
                            <dd>450.00€</dd>
                            <dt>
                                <input type="radio" id="distribucion_armarios_7" name="distribucion_armarios_2[]" value="Opción 3">
                                <label for="distribucion_armarios_7">Opción 3</label>
                            </dt>
                            <dd>450.00€</dd>
                            <dt>
                                <input type="radio" id="distribucion_armarios_8" name="distribucion_armarios_2[]" value="Opción 4">
                                <label for="distribucion_armarios_8">Opción 4</label>
                            </dt>
                            <dd>450.00€</dd>
                        </dl>
                    </div>
                    <div class="col-md-2 totales">
                        <p id="puertas_total" class="total text-right">0.00 €</p>
                    </div>
                </div><!-- .row -->
            </section>
            
            <section id="totales">        
                <div class="row">
                    <div class="col-md-9">
                        <p class="text-right">Importe total sin IVA</p>
                    </div>
                    <div class="col-md-2 totales">
                        <p class="total text-right">0.00 €</p>
                    </div>
                </div><!-- .row -->
                <div class="row">
                    <div class="col-md-9">
                        <p class="text-right">Importe total con IVA</p>
                    </div>
                    <div class="col-md-2 totales">
                        <p class="total text-right">0.00 €</p>
                    </div>
                </div><!-- .row -->

                <div class="row" style="margin-top: 20px;">
                    <div class="col-md-9">
                        <p class="imprimir text-right"><a href="javascript:if(window.print)window.print()">Imprimir</a></p>
                    </div>
                    <div class="col-md-2 totales">
                        <p class="text-right"><input type="submit" name="confirmar" value="confirmar" class="btn btn-primary"></p>
                    </div>
                </div><!-- .row -->
            </section>
    
    
            </form>

        </div><!-- .container -->
    </article>
    <?php include('../inc/pie.php'); ?>
  </body>
</html>





