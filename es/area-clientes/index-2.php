<?php include('../inc/init.php'); ?>
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        
        <title>Miramón Berri - Área de clientes</title>
        
        <link href='https://fonts.googleapis.com/css?family=Lato:300,400|Raleway' rel='stylesheet' type='text/css'>
        <link href="<?php echo $base_url; ?>/css/bootstrap.min.css" rel="stylesheet">
        <link href="<?php echo $base_url; ?>/css/main.css" rel="stylesheet">
        
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        
    </head>
    <body>
    <?php include('../inc/cabecera-clientes.php'); ?>
    
    <article id="contenido" class="area-clientes">
    <div class="fondo-gris">
        <div class="container">
        	<div class="row">
        	    <div class="col-lg-6 col-lg-offset-3 col-md-6 col-md-offset-3">
        	    	<h2 class="text-uppercase text-center">Bienvenido al área clientes de amenabar</h2>
        	    </div><!-- .col-md-8 -->
        	</div><!-- .row -->
        	<div class="row">
                <div class="col-md-12">
                	<p class="text-center">Un servicio exclusivo para los propietarios de viviendas de Construcciones Amenabar.</p>
                </div><!-- .col-md-12 -->
        	</div><!-- .row -->
        	<div class="row">
                <div class="col-md-12 margen-inferior margen-superior">
                	<p class="text-center"><a href="personaliza.php" class="btn btn-primary">Personaliza tu vivienda</a></p>
                </div><!-- .col-md-12 -->
        	</div><!-- .row -->
        </div>
    </div>
    <div class="container">
    	<div class="row">
            <div class="col-md-8 col-md-offset-2">
        	    <h2 class="text-uppercase text-center">Antes, durante y después.<br />Te acompañamos en todo el proceso de compra de tu nueva casa.</h2>
            </div>
        </div>
    	<div class="row">
            <div class="col-md-8 col-md-offset-2">
            	<p class="text-center">Sabemos lo importante que es la compra de una nueva casa para ti y para tu familia. Por eso queremos ayudarte en todo lo que necesites ofreciéndote una atención personalizada y un completo asesoramiento. Desde el primer momento, durante el proceso de compra hasta la entrega de las llaves y atendiendo a cualquier necesidad una vez que hayas entrado a vivir en tu nueva casa.</p>
            </div><!-- .col-md-12 -->
    	</div><!-- .row -->
            
    </div><!-- .container -->

    </article>
    <?php include('../inc/pie.php'); ?>
  </body>
</html>





