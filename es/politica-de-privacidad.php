<?php include('inc/init.php'); ?>
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        
        <title>Política de Privacidad</title>
        
        <link href='https://fonts.googleapis.com/css?family=Lato:300,400|Raleway' rel='stylesheet' type='text/css'>
        <link href="<?php echo $base_url; ?>/css/bootstrap.min.css" rel="stylesheet">
        <link href="<?php echo $base_url; ?>/css/main.css" rel="stylesheet">
        
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        
    </head>
    <body>
    <?php 
        $seccion = "aviso-legal";
        include('inc/cabecera.php');
    ?>
    
    <article id="contenido" class="legal">
        <div class="container">
        	<div class="row">
        	    <div class="col-md-6 col-md-offset-3">
        	    	<h2 class="text-uppercase text-center">Política de privacidad</h2>
        	    </div><!-- .col-md-6 -->
        	</div><!-- .row -->
        	
        	<div class="row">
        	    <div class="col-md-12">
        	    <h3>Protección de datos de carácter personal</h3>

                <p>En cumplimiento de la Ley Orgánica 15/1999, de 13 de diciembre, de Protección de Datos de Carácter Personal (LOPD), le informamos que los datos personales que nos suministre a través de la página web www.barandiaranberri.com, serán tratados de forma confidencial y pasarán a formar parte de un fichero automatizado titularidad de AMENABAR PROMOCIONES RESIDENCIALES, SL, dirección calle Nafarroa 50, Zarautz, Guipúzcoa, que ha sido debidamente inscrito en la Agencia Española de Protección de Datos (www.agpd.es).</p>
                
                <p>Sus datos personales serán utilizados con la finalidad que se indique en las páginas donde se encuentre el formulario electrónico de recogida de datos personales. Asimismo, podrán ser conservados para contestar a su solicitud y para el envío (por canales ordinarios o electrónicos) de información relacionada con nuestra actividad que pueda resultar de su interés, hasta que nos indique lo contrario. Asimismo le informamos que puede ejercitar sus derechos de acceso, rectificación, cancelación y oposición con arreglo a lo previsto en la LOPD, enviando una carta certificada o forma equivalente que permita acreditar su recepción, junto con la fotocopia de su D.N.I., a la siguiente dirección: calle Nafarroa 50, 20800, Zarautz, Guipúzcoa.</p>
                
                <p>La no cumplimentación de los campos, indicados como obligatorios, que aparecen en cualquier formulario de registro electrónico, podrá tener como consecuencia que AMENABAR PROMOCIONES RESIDENCIALES, SL no pueda atender a su solicitud.</p>
                
                <p>AMENABAR PROMOCIONES RESIDENCIALES, SL no vende, cede, arrienda ni transmite de ningún modo, información o datos de carácter personal de sus Clientes/Usuarios a terceros.
                A los efectos de lo que se indica en el párrafo anterior, le informamos de que AMENABAR PROMOCIONES RESIDENCIALES, SL tiene instaladas cookies en sus sistemas.</p>
                
                <p>Las cookies son pequeños archivos de texto que almacena el navegador en el disco duro de su ordenador. Cuando navega a través de nuestra página web, nuestro servidor podrá reconocer la cookie y proporcionarnos información sobre su última visita. La mayoría de los navegadores aceptan la utilización de cookies de forma automática, pero puede configurar su navegador para ser avisado en su pantalla de ordenador de la recepción de cookies y poder impedir su instalación en su disco duro.</p>
                
                <p>El Usuario garantiza que los Datos Personales facilitados a AMENABAR PROMOCIONES RESIDENCIALES, SL son veraces y se hace responsable de comunicar cualquier modificación en los mismos. El Usuario será el único responsable de cualquier daño o perjuicio, directo o indirecto, que pudiera ocasionar a AMENABAR PROMOCIONES RESIDENCIALES, SL o a cualquier tercero a causa de la cumplimentación de los formularios con datos falsos, inexactos, incompletos o no actualizados.</p>
                
                <p>El Usuario no deberá incluir datos personales de terceros sin su consentimiento informado previo de lo establecido en la presente política de privacidad, siendo el único responsable de su inclusión.</p>
                
                <p>Ciertos servicios prestados por AMENABAR PROMOCIONES RESIDENCIALES, SL, pueden contener condiciones particulares con previsiones específicas en materia de protección de datos personales, siendo por tanto de aplicación las condiciones particulares de cada caso. En todo lo no regulado por las condiciones particulares será de aplicación supletoria las condiciones generales recogidas en la presente política de privacidad.</p>
                
                <p>Se advierte a los niños y jóvenes menores de 18 años que han de tener el permiso de sus padres o tutores para proporcionar datos personales en el sitio web de AMENABAR PROMOCIONES RESIDENCIALES, SL</p>
                
                <p>AMENABAR PROMOCIONES RESIDENCIALES, SL ha adoptado las medidas necesarias para evitar la alteración, pérdida, tratamiento o acceso no autorizado de los datos personales, habida cuenta en todo momento del estado de la tecnología, la naturaleza de los datos almacenados y los riesgos a que están expuestos, ya provengan de la acción humana o del medio físico o natural, no obstante, el Usuario debe ser consciente de que las medidas de seguridad en Internet no son inexpugnables.</p>
                
                <p>AMENABAR PROMOCIONES RESIDENCIALES, SL se reserva el derecho de modificar su política de privacidad de acuerdo a su criterio, o a causa de un cambio legislativo, jurisprudencial o en la práctica empresarial. Si AMENABAR PROMOCIONES RESIDENCIALES, SL introdujera alguna modificación, el nuevo texto será publicado en este mismo sitio web, donde el Usuario podrá tener conocimiento de la política de privacidad actual de AMENABAR PROMOCIONES RESIDENCIALES, SL En cualquier caso, la relación con los usuarios se regirá por las normas previstas en el momento preciso en que se accede al sitio web.</p>
                
                <p>La cumplimentación de los formularios incluidos en el sitio web, implica el consentimiento expreso del Usuario a la inclusión de sus datos de carácter personal en el referido fichero automatizado de AMENABAR PROMOCIONES RESIDENCIALES, SL con las finalidades indicadas.</p>
                </div><!-- .col-md-12 -->
        	</div><!-- .row -->
        </div><!-- .container -->
    </article>
    <?php include('inc/pie.php'); ?>
  </body>
</html>





