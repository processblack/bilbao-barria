<?php

include ("./includes/sesiones.php");
include ("./includes/conexion.php");

//L�gica del controlador

//Verificamos que la sesi�n de administrador est� activa, si no lo est�, lo enviamos a pantalla t_loginadmin

$idusuario = $_SESSION["idusuario"];

if (isset($_SESSION["idusuario"])==0){
	include ("./templates/t_loginadmin.php");
	exit();
}

//Fin l�gica del controlador
include ("./templates/t_admin.php");

?>