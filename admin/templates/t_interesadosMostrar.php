<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<!--
Released for free under a Creative Commons Attribution 3.0 License
-->
<html xmlns="http://www.w3.org/1999/xhtml">

<?
	$title = "Admin";
?>
<?php require ("./templates/t_head.php"); ?>

<body>
<div id="wrapper">


<?php require ("./templates/t_cabecera.php"); ?>

<div class="container">
<div>
	<h4>Formulario de Interesados</h4>
	<p>&nbsp;</p>
	<form action="interesadosEditar.php" method="POST" name="formulario">
	<input type="hidden" name="id" value="<?=$id?>">
	<? if (($_SESSION["rol"] == "ADMIN") || ($_SESSION["rol"] == "EDITAR")) { ?>
		<a href="interesadosBorrar.php?id=<?=$id?>">Borrar registro</a><br/>
	<?} ?>

	<table>
	
	<tr><td>Nombre:&nbsp;</td><td><input type="text" size = "54" maxlength="100" name="nombre" value="<?=$nombre?>"></td></tr>
	<tr><td>Primer Apellido / Segundo Apellido:&nbsp;</td><td><input type="text" size = "25" maxlength="100" name="apellidos" value="<?=$apellidos?>"></td></tr>	
	<tr><td>Email:&nbsp;</td><td><input type="text" size = "54" maxlength="100" name="email" value="<?=$email?>"></td></tr>
	<tr><td>Tel&eacute;fono:&nbsp;</td><td><input type="text" size = "9" maxlength="9" name="telefono" value="<?=$telefono?>">&nbsp;<i>introduce d&iacute;gitos sin espacios</i></td></tr>

	
	<tr><td colspan="2">&nbsp;</td></tr>
	
	
	<tr><td colspan="2" align="right"><br/></td></tr>
	</table>
	</form>
</div>

</div>

</div>

	
<?php require ("./templates/t_pie.php"); ?>	
	
	
</div>



</body>
</html>